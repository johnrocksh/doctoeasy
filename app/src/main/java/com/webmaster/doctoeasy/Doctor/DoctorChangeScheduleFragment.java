package com.webmaster.doctoeasy.Doctor;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;


import com.google.android.material.tabs.TabLayout;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.doctorshedule.DoctorScheduleManager;
import com.webmaster.doctoeasy.doctorshedule.FragmentListener;
import com.webmaster.doctoeasy.doctorshedule.ViewPagerAdapter;
//android.support.v4.app.FragmentStatePagerAdapter
/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DoctorChangeScheduleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DoctorChangeScheduleFragment extends Fragment implements FragmentListener {
    @Override
    public void onStop() {
        super.onStop();
        Log.i("schedule","DoctorChangeScheduleFragment onStop");

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i("schedule","DoctorChangeScheduleFragment onDestroy");

    }

    DoctorScheduleManager doctorScheduleManager;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public DoctorChangeScheduleFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DoctorChangeScheduleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DoctorChangeScheduleFragment newInstance(String param1, String param2) {
        DoctorChangeScheduleFragment fragment = new DoctorChangeScheduleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("schedule","DoctorChangeScheduleFragment onCreate");

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_change_shedule, container, false);
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btnBackFragment).setOnClickListener((v->{
            onBtnBackClick(v);
        }));


        doctorScheduleManager = new DoctorScheduleManager(view);

        //create TABLayout
        tabLayout = getView().findViewById(R.id.tablalyout);
        viewPager = getView().findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager(),this);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(viewPagerAdapter);


        Log.i("schedule","DoctorChangeScheduleFragment onViewCreated");

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("schedule","DoctorChangeScheduleFragment onResume");

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("schedule","DoctorChangeScheduleFragment onPause");

    }

    private void onBtnBackClick(View v) {
        getParentFragmentManager().popBackStack();

    }


    @Override
    public void onUpdateSchedule(String weekDay, int[] buttonsState) {
        doctorScheduleManager.onUpdateSchedule(getContext(), weekDay, buttonsState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();


    }
}