package com.webmaster.doctoeasy.Doctor;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.webmaster.doctoeasy.Constants;
import com.webmaster.doctoeasy.Healthcheck.HealthCheckFragment;
import com.webmaster.doctoeasy.Patient.PatientAccountFragment;
import com.webmaster.doctoeasy.Patient.PatientAppointmentFragment;
import com.webmaster.doctoeasy.Patient.PatientInboxFragment;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.Search.SearchFragment;
import com.webmaster.doctoeasy.client.FakeClient;
import com.webmaster.doctoeasy.Patient.Patient;

import java.lang.reflect.Type;

public class DoctorAppointmentConfirmationFragment extends Fragment{

    Button btnCancel;
    Button btnConfirm;
    EditText date;
    EditText time;
    EditText firstName;
    EditText lastName;
    EditText dateOfBirth;
    EditText phoneNumber;
    EditText email;
    EditText cause;
    Patient patient;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.patient_newapointment_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnCancel=view.findViewById(R.id.btnCancel);
        btnConfirm=view.findViewById(R.id.btnConfirm);
            initFields(view);

        btnCancel.setOnClickListener((view1 -> {
            OnBtnCancelClick();
        }));

        btnConfirm.setOnClickListener((view1 -> {
            OnBtnConfirmClick();
        }));

        view.findViewById(R.id.btnBackFragment).setOnClickListener((v -> {
            onBackButtonClick();
        }));

        view.findViewById(R.id.btnPatientAccount).setOnClickListener((v -> {
            onAccountBtn(v);
        }));

        view.findViewById(R.id.btnPatientSearch).setOnClickListener((v) -> {
            onSearchBtn(v);
        });

        view.findViewById(R.id.btnPatientAppointment).setOnClickListener((v) -> {
            onAppointmentBtn(v);
        });

        view.findViewById(R.id.btnPatientInbox).setOnClickListener((v) -> {
            onInboxBtn(v);
        });

        view.findViewById(R.id.btnPatientHealthCheck).setOnClickListener((v) -> {
            onHealthBtn(v);
        });

    }

    private void initFields(View view) {

         date= view.findViewById(R.id.dateBooking);
         time=view.findViewById(R.id.timeBooking);
         firstName=view.findViewById(R.id.firstNameBooking);
         lastName=view.findViewById(R.id.lastNameBooking);
         dateOfBirth=view.findViewById(R.id.birthDayBooking);
         phoneNumber=view.findViewById(R.id.phoneNumberBooking);
         email=view.findViewById(R.id.emailBooking);
         cause=view.findViewById(R.id.etCauseBooking);

            String json = getArguments().getString("PatientInfo");
            Type listType= new TypeToken<Patient>(){}.getType();
             patient = new Gson().fromJson(json,listType);

                date.setText(patient.getDateOfBooking());
                time.setText(patient.getTimeOfBooking());
                firstName.setText(patient.getName());
                lastName.setText(patient.getLastName());
              //  dateOfBirth.setText(patient.getDateOfBirth());
                phoneNumber.setText(patient.getPhoneNumber());
                email.setText(patient.getEmail());
                cause.setText(patient.getCauseBooking());
    }

    private void OnBtnConfirmClick() {
        FakeClient.getClient(getContext()).setConfirmPatientAppointment(Patient.Status.CONFIRMED,getContext(),patient, Constants.prefName);
        Toast.makeText(getContext(), "Confirm", Toast.LENGTH_SHORT).show();
    }

    private void OnBtnCancelClick() {
        FakeClient.getClient(getContext()).setConfirmPatientAppointment(Patient.Status.CANCELED,getContext(),patient, Constants.prefName);

        Toast.makeText(getContext(), "Cancel", Toast.LENGTH_SHORT).show();

    }


    private void onAccountBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new PatientAccountFragment())
                .addToBackStack("PatietnAccountFragment")
                .commit();
    }

    private void onHealthBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new HealthCheckFragment())
                .addToBackStack("HealthCheckFragment")
                .commit();
    }

    private void onInboxBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new PatientInboxFragment())
                .addToBackStack("PatientInboxFragment")
                .commit();
    }


    private void onSearchBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new SearchFragment())
                .addToBackStack("SearchFragment")
                .commit();
    }

    private void onAppointmentBtn(View v) {

        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new PatientAppointmentFragment())
                .addToBackStack("PatientAppointmentFragment")
                .commit();
    }

    private void onBackButtonClick() {
        getParentFragmentManager().popBackStack();
    }


}
