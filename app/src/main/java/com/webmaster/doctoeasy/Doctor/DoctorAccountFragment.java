package com.webmaster.doctoeasy.Doctor;

import static android.app.Activity.RESULT_OK;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.webmaster.doctoeasy.Constants;
import com.webmaster.doctoeasy.Healthcheck.HealthCheckFragment;
import com.webmaster.doctoeasy.Main.MainFragment;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.Search.SearchFragment;
import com.webmaster.doctoeasy.client.FakeClient;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class DoctorAccountFragment extends Fragment {

    ImageView ivDoctorPhoto;
    private static final int PICK_IMAGE = 100;
    Uri imageUri;
    static Doctor doctor;
    EditText firstName;
    EditText lastName;
    EditText email;
    EditText telephone;
    EditText login;
    EditText password;

    public DoctorAccountFragment() {

        // Required empty public constructor
    }

    public static DoctorAccountFragment newInstance(String param1, String param2) {
        DoctorAccountFragment fragment = new DoctorAccountFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_doctor_account, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        firstName=view.findViewById(R.id.etFirstNameDoctor);
        lastName=view.findViewById(R.id.etSurnameDoctor);
        email=view.findViewById(R.id.etEmailDoctor);
        telephone=view.findViewById(R.id.etTelDoctor);
        login=view.findViewById(R.id.etLoginDoctor);



        ivDoctorPhoto = view.findViewById(R.id.ivDoctorPhoto);
        String doctorLogin = getArguments().getString("login");
        String doctorPassword = getArguments().getString("password");
        Log.i("LOG", "login: "+doctorLogin);
        Log.i("LOG", "password: "+doctorPassword);
        doctor=FakeClient.getClient(getContext()).getDoctor(getContext(),doctorLogin,doctorPassword);

        initDoctorAccount(doctor);
        view.findViewById(R.id.btnDoctorUploadPhoto).setOnClickListener((v -> {
            onClickUploadPhoto(v);
        }));

        view.findViewById(R.id.btnBackFragment).setOnClickListener((v -> {
            onBackButtonClick(v);
        }));

        view.findViewById(R.id.btnDoctorSignOut).setOnClickListener((v -> {
            onBtnSignOutClick(v);
        }));

        /////////////////////работа главного меню

        view.findViewById(R.id.btnDoctorAccount).setOnClickListener((v->{
            onMenuAccountClick(v);
        }));

        view.findViewById(R.id.btnDoctorBillings).setOnClickListener((v) -> {
            onMenuBillingsClick(v);
        });

        view.findViewById(R.id.btnDoctorReviews).setOnClickListener((v) -> {
            onMenuReviewsClick(v);
        });

        view.findViewById(R.id.btnDoctorInbox).setOnClickListener((v) -> {
            onMenuInboxClick(v);
        });

        view.findViewById(R.id.btnDoctorCalendar).setOnClickListener((v) -> {
            onMenuCalendarClick(v);
        });
        ////////////////////


    }


    private void onMenuAccountClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorAccountPropertiesFragment())
                .addToBackStack("DoctorAccountFragment")
                .commit();
    }

    private void onMenuCalendarClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorCalendarFragment())
                .addToBackStack("DoctorCalendarFragment")
                .commit();
    }

    private void onMenuInboxClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorInboxFragment())
                .addToBackStack("DoctorInboxFragment")
                .commit();
    }

    private void onMenuReviewsClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorReviewsFragment())
                .addToBackStack("DoctorReviewFragment")
                .commit();
    }

    private void onMenuBillingsClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new DoctorAppointmentFragment())
                .addToBackStack("DoctorAppointmentFragment")
                .commit();
    }

        private void initDoctorAccount(Doctor doctor) {

        firstName.setText(doctor.getName());
        lastName.setText(doctor.getLastName());
        telephone.setText(doctor.getPhoneNumber());
        email.setText(doctor.getEmail());
        login.setText(doctor.getLogin());
        setPhoto(doctor.getPhotoPath());
    }

    private void setPhoto(String photoPath) {

        if(photoPath!=null){
            File imgFile = new  File(doctor.getPhotoPath());
            if(imgFile.exists()){

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                ivDoctorPhoto.setImageBitmap(myBitmap);
            }
        }
    }

    private void onClickUploadPhoto(View v) {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imageUri = data.getData();
            ivDoctorPhoto.setImageURI(imageUri);
            String fileName = getFileName(imageUri);
            String realPath=getRealPathFromURI(getContext(),imageUri);
            savePhoto(imageUri,realPath, fileName);
        }
    }

    private String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            Log.e("LOG", "getRealPathFromURI Exception : " + e.toString());
            return "";
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

//    public String getFilePath(Uri uri){
//
//        File file = new File(uri.getPath());//create path from uri
//        final String[] split = file.getPath().split(":");//split the path.
//        String filePath = split[1];//assign it to a string(your choice).
//    return  filePath;
//    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void savePhoto(Uri uri,String filePath, String fileName) {
        Log.i("LOG", "savePhoto: "+filePath+fileName);
        //first create folder then..
        File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+File.separator+ Constants.dirDoctorPhoto);
        Log.i("LOG", "newFolder: "+folder.getPath());

        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        if (success) {

            try {
                String doctorPhoto=filePath;
                Context context=getContext();
                copyFile(context, doctorPhoto,folder);
            } catch (IOException e) {
                e.printStackTrace();
                Log.i("LOG", "IOException: "+e);
            }

        } else {
            Toast.makeText(getContext(), "DoctorEasy can't create the Folder!", Toast.LENGTH_SHORT).show();
        }
    }

    //////////////
    //copyFile
    /////////////
    @RequiresApi(api = Build.VERSION_CODES.O)
    private static void copyFile(Context context,String doctorPhoto, File newFolder) throws IOException {
        //from
        File originalFile=new File(doctorPhoto);
        Path pathOut = (Path)Paths.get(originalFile.getPath());
        //to
        Path pathIn = Paths.get(newFolder.getPath()+"/"+doctor.getName()+doctor.getLogin()+".jpg");
        // !!!//call the update method for doctor in Fake client and save photo path
        FakeClient.getClient(context).saveDoctorPhotoPath(context,doctor,pathIn);
        Files.copy(pathOut, pathIn, StandardCopyOption.REPLACE_EXISTING);
    }


    private void onBtnSignOutClick(View v) {

        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new MainFragment())
                .commit();
    }

    private void onBackButtonClick(View v) {
        FragmentManager fm = getParentFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
        } else {
            Log.i("Log", "nothing on backstack, calling super");
        }
    }

    private void onHealthBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new HealthCheckFragment())
                .addToBackStack("HealthCheckFragment")
                .commit();
    }

    private void onInboxBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorInboxFragment())
                .addToBackStack("doctorInboxFragment")
                .commit();
    }



    private void onSearchBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new SearchFragment())
                .addToBackStack("SearchFragment")
                .commit();
    }

    private void onAppointmentBtn(View v) {

        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorAppointmentFragment())
                .addToBackStack("doctorAppointmentFragment")
                .commit();
    }
}
