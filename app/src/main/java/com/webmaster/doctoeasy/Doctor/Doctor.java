package com.webmaster.doctoeasy.Doctor;

import com.webmaster.doctoeasy.Patient.Patient;

import java.util.ArrayList;
// specialists.add(new Specialist(photoSpecialist,name,specialisation,starRating,view.findViewById(R.id.btnBookPatient)));
//
public class Doctor {

    private int id;
    private String name;
    private String lastName;
    private String specialization;
    private String city;
    private Patient.Gender gender;
    private String dateOfBirth;
    private String phoneNumber;
    private String email;
    private String photoPath;
    private String login;
    private String password;

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    private boolean registered;


    public enum Gender{
        FEMALE,
        MALE
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<String> weekDay;

    public ArrayList<String> getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(ArrayList<String> weekDay) {
        this.weekDay = weekDay;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Patient.Gender getGender() {
        return gender;
    }

    public void setGender(Patient.Gender gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }



    public  Doctor(String name, String lastName, String specialization,String city,Patient.Gender gender, String dateOfBirth, String phoneNumber, String email,
                    String login, String password){

        this.name= name;
        this.lastName=lastName;
        this.specialization=specialization;
        this.city=city;
        this.gender=gender;
        this.dateOfBirth=dateOfBirth;
        this.phoneNumber=phoneNumber;
        this.email=email;
        this.login=login;
        this.password=password;
        registered=true;

    }

    public   Doctor(){
        weekDay=new ArrayList<String>();
        weekDay.add("Monday");
        weekDay.add("Tuesday");
        weekDay.add("Wednesday");
        weekDay.add("Thursday");
        weekDay.add("Friday");
        weekDay.add("Saturday");
        weekDay.add("Sunday");
    }

}
