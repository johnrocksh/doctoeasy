package com.webmaster.doctoeasy.Doctor;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.webmaster.doctoeasy.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DoctorAccountPropertiesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DoctorAccountPropertiesFragment extends Fragment {

    public static DoctorAccountPropertiesFragment newInstance(String param1, String param2) {
        DoctorAccountPropertiesFragment fragment = new DoctorAccountPropertiesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        view.findViewById(R.id.btnBackFragment).setOnClickListener((v->{
            onBackBtnClick(v);
        }));

        view.findViewById(R.id.btnLocation).setOnClickListener((v->{
            onBtnLocationClick(v);
        }));

        view.findViewById(R.id.btnServices).setOnClickListener((v->{
            onBtnServicesClick(v);
        }));

        view.findViewById(R.id.btnProfile).setOnClickListener((v->{
            onBtnProfilesClick(v);
        }));

        view.findViewById(R.id.btnSettings).setOnClickListener((v->{
            onBtnSettingsClick(v);
        }));

        /////////////////////работа главного меню


        view.findViewById(R.id.btnDoctorBillings).setOnClickListener((v) -> {
            onMenuBillingsClick(v);
        });

        view.findViewById(R.id.btnDoctorReviews).setOnClickListener((v) -> {
            onMenuReviewsClick(v);
        });

        view.findViewById(R.id.btnDoctorInbox).setOnClickListener((v) -> {
            onMenuInboxClick(v);
        });

        view.findViewById(R.id.btnDoctorCalendar).setOnClickListener((v) -> {
            onMenuCalendarClick(v);
        });
        ////////////////////


    }

    private void onMenuCalendarClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorCalendarFragment())
                .addToBackStack("DoctorCalendarFragment")
                .commit();
    }

    private void onMenuInboxClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorInboxFragment())
                .addToBackStack("DoctorInboxFragment")
                .commit();
    }

    private void onMenuReviewsClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorReviewsFragment())
                .addToBackStack("DoctorReviewFragment")
                .commit();
    }

    private void onMenuBillingsClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorAppointmentFragment())
                .addToBackStack("DoctorBillingsFragment")
                .commit();
    }


    private void onBtnSettingsClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorSettingsFragment())
                .addToBackStack("DoctorSettingsFragment")
                .commit();
    }

    private void onBtnProfilesClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorProfileFragment())
                .addToBackStack("DoctorProfileFragment")
                .commit();
    }

    private void onBtnServicesClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorServicesFragment())
                .addToBackStack("DoctorServicesFragment")
                .commit();
    }

    private void onBtnLocationClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorLocationFragment())
                .addToBackStack("DoctorLocationFragment")
                .commit();
    }

    private void onBackBtnClick(View v) {
        getParentFragmentManager().popBackStack();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_doctor_account_properties, container, false);
    }
}