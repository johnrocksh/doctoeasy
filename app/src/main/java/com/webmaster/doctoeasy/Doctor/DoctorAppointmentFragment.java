package com.webmaster.doctoeasy.Doctor;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.webmaster.doctoeasy.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DoctorAppointmentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DoctorAppointmentFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public DoctorAppointmentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DoctorAppointmentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DoctorAppointmentFragment newInstance(String param1, String param2) {
        DoctorAppointmentFragment fragment = new DoctorAppointmentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.topPanelBackButton).setOnClickListener((v->{
            onBtnBackClick(v);
        }));

        /////////////////////работа главного меню

        view.findViewById(R.id.btnDoctorAccount).setOnClickListener((v->{
            onMenuAccountClick(v);
        }));

        view.findViewById(R.id.btnDoctorBillings).setOnClickListener((v) -> {
            onMenuBillingsClick(v);
        });

        view.findViewById(R.id.btnDoctorReviews).setOnClickListener((v) -> {
            onMenuReviewsClick(v);
        });

        view.findViewById(R.id.btnDoctorInbox).setOnClickListener((v) -> {
            onMenuInboxClick(v);
        });

        view.findViewById(R.id.btnDoctorCalendar).setOnClickListener((v) -> {
            onMenuCalendarClick(v);
        });
        ////////////////////

    }

    private void onMenuAccountClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorAccountPropertiesFragment())
                .addToBackStack("DoctorAccountFragment")
                .commit();
    }

    private void onMenuCalendarClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorCalendarFragment())
                .addToBackStack("DoctorCalendarFragment")
                .commit();
    }

    private void onMenuInboxClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorInboxFragment())
                .addToBackStack("DoctorInboxFragment")
                .commit();
    }

    private void onMenuReviewsClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorReviewsFragment())
                .addToBackStack("DoctorReviewFragment")
                .commit();
    }

    private void onMenuBillingsClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorAppointmentFragment())
                .addToBackStack("DoctorAppointmentFragment")
                .commit();
    }

    private void onBtnBackClick(View v) {
        getParentFragmentManager().popBackStack();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_doctor_billings, container, false);
    }
}