package com.webmaster.doctoeasy.Doctor;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.webmaster.doctoeasy.R;


public class DoctorNewAppointmentFragment extends Fragment {



    public DoctorNewAppointmentFragment() {
        // Required empty public constructor
    }


    public static DoctorNewAppointmentFragment newInstance(String param1, String param2) {
        DoctorNewAppointmentFragment fragment = new DoctorNewAppointmentFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_doctor_new_appointment, container, false);
    }
}