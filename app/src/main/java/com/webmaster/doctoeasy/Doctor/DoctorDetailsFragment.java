package com.webmaster.doctoeasy.Doctor;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.webmaster.doctoeasy.Healthcheck.HealthCheckFragment;
import com.webmaster.doctoeasy.Patient.PatientAccountFragment;
import com.webmaster.doctoeasy.Patient.PatientAppointmentFragment;
import com.webmaster.doctoeasy.Patient.PatientBookingFragment;
import com.webmaster.doctoeasy.Patient.PatientInboxFragment;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.Search.SearchFragment;
import com.webmaster.doctoeasy.Specialist;
import com.webmaster.doctoeasy.client.FakeClient;

import java.util.ArrayList;

public class DoctorDetailsFragment extends Fragment implements OnMapReadyCallback {


    public   SupportMapFragment  mapFragment ;
    String address;
    LatLng Country;
    private GoogleMap googleMap;
    Bundle bundle;

    public DoctorDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.i("LOG","onAttach");
    }


    public static DoctorDetailsFragment newInstance(String param1, String param2) {
        DoctorDetailsFragment fragment = new DoctorDetailsFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
          }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_doctor_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bundle=new Bundle();
        /////////////////////// DOCTORS PERSONAL INFORMATION
        //Retrieve the value from Bundle
        String value = getArguments().getString("position");
        Integer position= Integer.parseInt(value);//position of selected item in DoctorListView

        //get specialist information from FakeClient
        ArrayList<Specialist> specialistArrayList;
        specialistArrayList= FakeClient.getClient(getContext()).getDoctorsList(getContext(),view);

         Specialist specialist=specialistArrayList.get(position);
         String name=specialist.getName();
         String loginDoctors=specialist.getLogin();
         Drawable photo=specialist.getPhoto();
         String specialisation=specialist.getSpecialisation();
         address=specialist.getAddress();
         Float latitude=specialist.getLatitude();
         Float longitude=specialist.getLongitude();
         int starsCount=specialist.getStarsCount();
         Log.i("LOG","starsCount:"+starsCount);

         Country= new LatLng(latitude, longitude);
        bundle.putString("loginDoctors",loginDoctors);

         //get views from resources file
        ImageView specialistPhoto=view.findViewById(R.id.imageViewSpecialistPhoto);
        TextView specialistName=view.findViewById(R.id.textViewSpecialistName);
        TextView specialistSpecialisation=view.findViewById(R.id.textViewSpetialisation);
        TextView specialistAddress=view.findViewById(R.id.doctorDetailsAddress);

         setRating(view,getContext(),starsCount);

        //set geted data to view fields
        specialistPhoto.setImageDrawable(photo);
        specialistName.setText(name);
        specialistSpecialisation.setText(specialisation);
        specialistAddress.setText(address);

        //////////////////GOOGLE MAPS
        Log.i("LOG:", "on viewCreated  getParentFragmentManager:"+getParentFragmentManager());
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.my_map);
          mapFragment.getMapAsync(this);


        ///////////////////////CLICK LISTENERS
        view.findViewById(R.id.btnBackFragment).setOnClickListener((v->{
            getParentFragmentManager().popBackStack();

        }));

        view.findViewById(R.id.textViewShfedule).setOnClickListener((v->{
            onServicesClick(v);
        }));

        view.findViewById(R.id.textViewShedule).setOnClickListener((v->{
            onSheduleClick(v);
        }));

        view.findViewById(R.id.textViewAbout).setOnClickListener((v->{
            onAboutClick(v);
        }));

       view.findViewById(R.id.btnDoctorReviews).setOnClickListener((v->{
           onBtnDoctorReviewsClick(v);
       }));

        view.findViewById(R.id.btnBook).setOnClickListener((v->{
            onBtnBookClick(v);
        }));

        view.findViewById(R.id.btnPatientAccount).setOnClickListener((v->{
            onAccountBtn(v);
        }));

        view.findViewById(R.id.btnPatientSearch).setOnClickListener((v) -> {
            onSearchBtn(v);
        });

        view.findViewById(R.id.btnPatientAppointment).setOnClickListener((v) -> {
            onAppointmentBtn(v);
        });

        view.findViewById(R.id.btnPatientInbox).setOnClickListener((v) -> {
            onInboxBtn(v);
        });

        view.findViewById(R.id.btnPatientHealthCheck).setOnClickListener((v) -> {
            onHealthBtn(v);
        });


    }

    private void setRating(View view,Context context,int starsCount) {
           //GET DRAWABLE FROM RESOURCE FILE
           Drawable star=context.getResources().getDrawable(R.drawable.star);
           Drawable grey_star=context.getResources().getDrawable(R.drawable.rating_star_grey);
           LinearLayout linearLayoutRating=view.findViewById(R.id.linearLayoutRating);
        int childCount=linearLayoutRating.getChildCount();

    for(int i=0;i<childCount;i++){
     if(i<=starsCount){
        ImageView v=(ImageView)   linearLayoutRating.getChildAt(i);
        v.setImageDrawable(star);
}
     else{
         ImageView v=(ImageView)   linearLayoutRating.getChildAt(i);
         v.setImageDrawable(grey_star);
    }
  }
 }


    private void onAboutClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorAboutFragment())
                .addToBackStack("DoctorAboutFragment")
                .commit();
    }

    private void onSheduleClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorSheduleFragment())
                .addToBackStack("DoctorSheduleFragment")
                .commit();
    }

    private void onServicesClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorServicesFragment())
                .addToBackStack("DoctorServicesFragment")
                .commit();
    }


    private void onBtnDoctorReviewsClick(View v) {

        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorReviewsFragment())
                .addToBackStack("DoctorReviewsFragment")
                .commit();
    }

    private void onAccountBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientAccountFragment())
                .addToBackStack("PatietnAccountFragment")
                .commit();
    }

    private void onHealthBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new HealthCheckFragment())
                .addToBackStack("HealthCheckFragment")
                .commit();
    }

    private void onInboxBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientInboxFragment())
                .addToBackStack("PatientInboxFragment")
                .commit();
    }



    private void onSearchBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new SearchFragment())
                .addToBackStack("SearchFragment")
                .commit();
    }

    private void onAppointmentBtn(View v) {

        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientAppointmentFragment())
                .addToBackStack("PatientAppointmentFragment")
                .commit();
    }

    private void onBtnBookClick(View v) {
        Fragment fr=new PatientBookingFragment();
        fr.setArguments(bundle);
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,fr)
                .addToBackStack("PatientBookingFragment")
                .commit();

    }


    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setTrafficEnabled(true);
        googleMap.setIndoorEnabled(true);
        googleMap.setBuildingsEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.addMarker(new MarkerOptions()
                .position(Country)
                .snippet(address)
                .title(address));


          //////////////////////
       CameraPosition cameraPosition=new CameraPosition.Builder()
               .target(Country)
               .zoom(2)
              // .bearing(45)
              // .tilt(20)
               .build();
       CameraUpdate cameraUpdate= CameraUpdateFactory.newCameraPosition(cameraPosition);
      map.moveCamera(cameraUpdate);
        ////////////////////////

Log.i("LOG","Country:"+Country);

    }
}