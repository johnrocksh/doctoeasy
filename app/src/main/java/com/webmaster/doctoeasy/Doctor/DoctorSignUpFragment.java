package com.webmaster.doctoeasy.Doctor;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.webmaster.doctoeasy.Login.LoginFragment;
import com.webmaster.doctoeasy.Patient.Patient;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.client.FakeClient;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DoctorSignUpFragment extends Fragment implements AdapterView.OnItemSelectedListener {


    TextView name;
    TextView lastName;
    TextView specialization;
    TextView city;
    TextView phoneNumber;
    TextView email;
    TextView login;
    TextView password;
    TextView dateOfBirth;
    RadioButton female;
    RadioButton male;
    Spinner spinBySpecialization;
    Spinner spinByLocation;
    Patient.Gender gender = Patient.Gender.MALE;
    final Calendar myCalendar = Calendar.getInstance();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_sign_up_specialist, container, false);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){

        name = view.findViewById(R.id.etSignUpDoctorName);
        lastName = view.findViewById(R.id.etSignUpDoctorLastName);
        phoneNumber = view.findViewById(R.id.etDoctorPhoneNumber);
        email = view.findViewById(R.id.etDoctorEmail);
        login = view.findViewById(R.id.etDoctorLogin);
        password = view.findViewById(R.id.etDoctorPassword);
        dateOfBirth = view.findViewById(R.id.etDoctorDateOfBirth);
        female=view.findViewById(R.id.rbDoctorGenderMale);
        male=view.findViewById(R.id.rbDoctorGenderMale);
//        specialization=view.findViewById(R.id.etSignUpDoctorSpecialization);
//        city=view.findViewById(R.id.etSignUpDoctorCity);

        spinBySpecialization=view.findViewById(R.id.spinnerSearchBySpecialist);
        spinByLocation=view.findViewById(R.id.spinnerSearchByLocation);

        //SEARCH BY CPECIALIST
        ArrayAdapter<String> adapterSpecialist=new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, FakeClient.getClient(getContext()).doctorsSpecializations);
        adapterSpecialist.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinBySpecialization.setAdapter(adapterSpecialist);
        spinBySpecialization.setOnItemSelectedListener(this);
        //SEARCH BY CITY
        ArrayAdapter<String>adapterLocation=new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, FakeClient.getClient(getContext()).doctorLocations);
        adapterLocation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinByLocation.setAdapter(adapterLocation);
        spinByLocation.setOnItemSelectedListener(this);

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, day);
                updateLabel();
            }

            private void updateLabel() {
                String myFormat = "MM.dd.yy";
                SimpleDateFormat dateFormat = new SimpleDateFormat(myFormat, Locale.US);
                dateOfBirth.setText(dateFormat.format(myCalendar.getTime()));
            }

        };



        view.findViewById(R.id.btnBackSignUp).setOnClickListener((v) -> {
            onArrowBackBtn(v);
        });

        dateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(), date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        view.findViewById(R.id.btn_SignUpDoctor).setOnClickListener((v) -> {
            onSignUpDoctorBtn(v);
        });


        view.findViewById(R.id.btnBackSignUp).setOnClickListener((v) -> {
            onArrowBackBtn(v);
        });
    }


    public void onArrowBackBtn(View view) {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    private void onSignUpDoctorBtn(View v) {


            if (female.isChecked()) {
                gender = Patient.Gender.FEMALE;
            } else if (male.isChecked()) {
                gender = Patient.Gender.MALE;
            }
            String s_name =name.getText().toString();
            String s_lname =lastName.getText().toString();
            String s_phoneNumber =phoneNumber.getText().toString();
            String s_email =email.getText().toString();
            String s_login =login.getText().toString();
            String s_pass =password.getText().toString();
            String s_dateOfBirth =dateOfBirth.getText().toString();
            String s_specialization=specialization.getText().toString();
            String s_city=city.getText().toString();

            Doctor doctor = new Doctor(
                    s_name,
                    s_lname,
                    s_specialization,
                    s_city,
                    gender,
                    s_dateOfBirth,
                    s_phoneNumber,
                    s_email,
                    s_login,
                    s_pass
            );
            FakeClient.getClient(getContext()).newDoctorRegistration(getContext(),doctor);
            Toast.makeText(getContext(), "Successful Registered Doctor!", Toast.LENGTH_SHORT).show();
            getParentFragmentManager().beginTransaction()
                    .replace(R.id.fragments_container,new LoginFragment())
                    .addToBackStack("LoginFragment")
                    .commit();

        }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}


