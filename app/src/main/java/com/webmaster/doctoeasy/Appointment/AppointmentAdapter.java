package com.webmaster.doctoeasy.Appointment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.webmaster.doctoeasy.R;

import java.util.List;

public class AppointmentAdapter extends ArrayAdapter<Appointment> {

    private LayoutInflater infl;
    private int resource;
    private List<Appointment> appointments;


    public AppointmentAdapter(Context context, int resource, List<Appointment> appointments) {
        super(context, resource, appointments);
        this.infl= LayoutInflater.from(context);
        this.resource=resource;
        this.appointments=appointments;

    }

    public View getView(int position, View convertView, ViewGroup parent){

        ViewHolder viewHolder;
        if(convertView==null){

            convertView=infl.inflate(this.resource,parent,false);
            viewHolder= new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        else
            viewHolder=(ViewHolder)convertView.getTag();

        final Appointment appointment=appointments.get(position);

        viewHolder.textViewTime.setText(appointment.getTime());
        viewHolder.textViewFirstName.setText(appointment.getName());
        viewHolder.textViewLastName.setText(appointment.getLastName());
        viewHolder.textViewStatus.setText(appointment.getStatus());
        viewHolder.imageViewStatus.setImageDrawable(appointment.getImgStatus());

        return convertView;
    }

    private class ViewHolder{

        final ImageView imageViewClinic;
        final ImageView imageViewStatus;
        final TextView textViewTime;
        final TextView  textViewFirstName;
        final TextView  textViewLastName;
        final TextView   textViewStatus;

        ViewHolder(View view){

             imageViewClinic = view.findViewById(R.id.imageViewClinic);
             textViewTime =  view.findViewById(R.id.textViewTime);
             textViewFirstName= view.findViewById(R.id.textViewFirstName);
             textViewLastName=  view.findViewById(R.id.textViewLastName);
             imageViewStatus= view.findViewById(R.id.imageViewStatus);
             textViewStatus= view.findViewById(R.id.textViewStatus);

        }
    }
}
