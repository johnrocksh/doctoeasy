package com.webmaster.doctoeasy.Appointment;

import android.graphics.drawable.Drawable;

public class Appointment {
    private Drawable img_clinic;
    private String str_time;
    private String str_name;
    private String str_lastname;
    private String str_status;
    private Drawable img_status;


    public Appointment(Drawable img_clinic, String str_time, String str_name, String str_lastname, String str_status, Drawable img_status) {

        this.img_clinic = img_clinic;
        this.str_time = str_time;
        this.str_name = str_name;
        this.str_lastname = str_lastname;
        this.str_status = str_status;
          this.img_status = img_status;
    }

    public Drawable getClinic() {
        return img_clinic;
    }

    public String getTime() {
        return str_time;
    }
    public String getName() {
        return str_name;
    }
    public String getLastName() {return str_lastname; }
    public void setName(String str_name) {
        this.str_name = str_name;
    }
    public void setLastName(String str_lastname) { this.str_lastname = str_lastname; }
    public Drawable getImgStatus() {
        return img_status;
    }
    public String getStatus() {
        return str_status;
    }
    public void setClinic(Drawable img_clinic) {
        this.img_clinic = img_clinic;
    }
    public void setStatus(Drawable img_status) {
        this.img_status = img_status;
    }
    public void setTime(String str_time) {
        this.str_time = str_time;
    }
    public void setStatus(String str_status) {
        this.str_status = str_status;
    }
}
