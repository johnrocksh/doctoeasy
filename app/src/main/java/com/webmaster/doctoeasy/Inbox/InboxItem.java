package com.webmaster.doctoeasy.Inbox;

import android.graphics.drawable.Drawable;

public class InboxItem {

    private Drawable status;
    private String name;
    private String description;
    private String date;
    private String time;

    public InboxItem(Drawable status, String name, String description, String date, String time) {

        this.date = date;
        this.description = description;
        this.name = name;
        this.time = time;
        this.status = status;
    }

    public Drawable getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public String getdescription() {
        return description;
    }

    public String getdate() {
        return date;
    }

    public String gettime() {
        return date;
    }

    public void setStatus(Drawable status) {
        this.status = status;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
