package com.webmaster.doctoeasy.Inbox;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.webmaster.doctoeasy.R;

import java.util.List;

public class InboxItemAdapter extends ArrayAdapter<InboxItem> {


    private LayoutInflater infl;
    private int resource;
    private List<InboxItem> inboxItems;

    public InboxItemAdapter(@NonNull Context context, int resource, @NonNull List<InboxItem> inboxItems) {
        super(context, resource, inboxItems);

        this.infl = LayoutInflater.from(context);
        this.resource = resource;
        this.inboxItems = inboxItems;

    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {

            convertView = infl.inflate(this.resource, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else
            viewHolder = (ViewHolder) convertView.getTag();

        final InboxItem inboxItem = inboxItems.get(position);
        viewHolder.status.setImageDrawable(inboxItem.getStatus());
        viewHolder.name.setText(inboxItem.getName());
        viewHolder.description.setText(inboxItem.getdescription());
        viewHolder.date.setText(inboxItem.getdate());
        viewHolder.date.setText(inboxItem.gettime());


        return convertView;
    }


    private class ViewHolder {

        final ImageView status;
        final TextView name;
        final TextView description;
        final TextView date;
        final TextView time;

        ViewHolder(View view) {

            status=view.findViewById(R.id.imageViewInboxItemStatus);
            name=view.findViewById(R.id.textViewSpetialisation);
            description=view.findViewById(R.id.textViewInboxItemDescription);
            date=view.findViewById(R.id.textViewdInboxItemDate);
            time=view.findViewById(R.id.textViewInboxItemTime);

        }

    }
}
