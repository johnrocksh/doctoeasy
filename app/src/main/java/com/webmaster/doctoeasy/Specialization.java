package com.webmaster.doctoeasy;

import android.graphics.drawable.Drawable;

public class Specialization {

   private String specialization;
   private Drawable  selected;

       public Specialization(String specialization, Drawable selected){

           this.specialization=specialization;
           this.selected=selected;
    }

    public String getItem(){

           return specialization;
    }

    public Drawable getSelected(){

           return selected;
    }

    public void setItem(String item){
           this.specialization=item;
    }

    public void setSelected(Drawable selected){

           this.selected=selected;
    }
}



