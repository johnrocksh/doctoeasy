package com.webmaster.doctoeasy.Patient;

import static androidx.constraintlayout.widget.Constraints.TAG;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.webmaster.doctoeasy.Healthcheck.HealthCheckFragment;
import com.webmaster.doctoeasy.Main.MainActivity;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.Search.SearchFragment;
import com.webmaster.doctoeasy.client.FakeClient;
import com.webmaster.doctoeasy.Doctor.Doctor;
import com.webmaster.doctoeasy.decorators.HighlightWeekendsDecorator;
import com.webmaster.doctoeasy.decorators.MySelectorDecorator;
import com.webmaster.doctoeasy.decorators.OneDayDecorator;
import com.webmaster.doctoeasy.doctorshedule.DayFragment;
import com.webmaster.doctoeasy.doctorshedule.DoctorScheduleManager;
import com.webmaster.doctoeasy.doctorshedule.FragmentListener;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;
import org.threeten.bp.Month;

import java.util.HashMap;

import butterknife.ButterKnife;


public class PatientBookingFragment extends Fragment implements FragmentListener, OnDateSelectedListener, AdapterView.OnItemSelectedListener  {

    private final OneDayDecorator oneDayDecorator = new OneDayDecorator();
    String[] causeOfConsultation = { "Consultation", "Survey", "Referal from work","Other"};
    EditText firstNameBooking;
    EditText lastNameBooking;
    EditText birthDayBooking;
    EditText phoneNumberBooking;
    EditText emailBooking;
    EditText timeBooking;
    Spinner spinCause;
    MainActivity ma;
    Button btnOk;
    int[] doctorScheduleButtonState = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    HashMap<Integer, String> hashMapWorkingHours = new HashMap<Integer, String>();
       String prefStr;
    SharedPreferences pref;
    DoctorScheduleManager doctorScheduleManager;
    DayFragment dayFragment;
    Doctor doctor;
    Patient patient;

    public PatientBookingFragment() {
        // Required empty public constructor
    }


    public static PatientBookingFragment newInstance() {
        PatientBookingFragment fragment = new PatientBookingFragment();
         return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_patient_booking, container, false);
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       //initialisation
        ma = (MainActivity) getActivity();
         patient =new Patient();
        Log.i("LOG", "patient:"+ patient);
        ButterKnife.bind(getActivity());

        btnOk=view.findViewById(R.id.btnOkPatientBooking);
        btnOk.setOnClickListener(view1 -> {
            onBtnClick();
        });

       //main logics
        singleRowCalendarCreation();
        initHashMapWorkHours();
        initPatientBookingFields(view, patient);

        // "getCurrentDay" return currentSelected day in correct mode for exm: "Monday"
        doctorScheduleButtonState= getDoctorScheduleButtonsState(view, getCurrentDay(ma.widget.getSelectedDate().getDate().toString()));
        //get preference if selected day is bu
        //
        addTimeButtonToScrollView(view, doctorScheduleButtonState);

        view.findViewById(R.id.btnBackFragment).setOnClickListener((v -> {
            onBackButtonClick();
        }));

        view.findViewById(R.id.btnPatientAccount).setOnClickListener((v -> {
            onAccountBtn(v);
        }));

        view.findViewById(R.id.btnPatientSearch).setOnClickListener((v) -> {
            onSearchBtn(v);
        });

        view.findViewById(R.id.btnPatientAppointment).setOnClickListener((v) -> {
            onAppointmentBtn(v);
        });

        view.findViewById(R.id.btnPatientInbox).setOnClickListener((v) -> {
            onInboxBtn(v);
        });

        view.findViewById(R.id.btnPatientHealthCheck).setOnClickListener((v) -> {
            onHealthBtn(v);
        });
    }

    public void onBtnClick() {
        Bundle mBundle = new Bundle();
        mBundle = getArguments();


        //fields validation
        Toast.makeText(getContext(), fieldsValidation(), Toast.LENGTH_SHORT).show();
        //call to fake klient method to save date of booking patient
        String strPref="ArrayListOfBookingPatient";
        //
        Log.i("LOG", "timeBooking: "+timeBooking.getText().toString());
        Log.i("LOG", "firstNameBooking: "+firstNameBooking.getText().toString());
        Log.i("LOG", "lastNameBooking: "+lastNameBooking.getText().toString());
        Log.i("LOG", "birthDayBooking: "+birthDayBooking.getText().toString());
        Log.i("LOG", "emailBooking: "+emailBooking.getText().toString());
        //

            patient.setDateOfBooking(ma.widget.getSelectedDate().getDate().toString());
            patient.setTimeOfBooking(timeBooking.getText().toString());
            patient.setName(firstNameBooking.getText().toString());
        patient.setLastName(lastNameBooking.getText().toString());
        patient.setDateOfBirth(birthDayBooking.getText().toString());
        patient.setPhoneNumber(phoneNumberBooking.getText().toString());
        patient.setEmail(emailBooking.getText().toString());
        patient.setStatus(Patient.Status.NEW);
        patient.setCauseBooking(spinCause.getSelectedItem().toString());
        patient.setLoginOfDoctorsBooking(mBundle.getString("loginDoctors"));
        Log.i("LOG", "onBtnClick patient:"+patient);

        clearTextFields();

        FakeClient.getClient(getContext()).patientBooking(getContext(), patient,strPref);

    }

    private void clearTextFields(){
        timeBooking.setText("");
        firstNameBooking.setText("");
        lastNameBooking.setText("");
        birthDayBooking.setText("");
        phoneNumberBooking.setText("");
        emailBooking.setText("");
    }
    private String fieldsValidation() {

        Log.i(TAG, "editTextTimeBooking"+timeBooking.getText());
        if(firstNameBooking.getText().length()==0)return "Please enter correct 'name'. ";
        if(birthDayBooking.getText().length()==0)return "Please enter 'birth Date'. ";
        if(lastNameBooking.getText().length()==0)return "Please enter 'Last name'. ";
        if(phoneNumberBooking.getText().length()==0)return "Please enter 'phone number'. ";
        if(timeBooking.getText().length()==0)return "Please select 'booking time'. ";
        if(emailBooking.getText().length()==0)return "Please enter 'correct email'. ";

        return "You have successfully booked an appointment!";
    }

    private void initPatientBookingFields(View view,Patient patient) {
//associate
        timeBooking=view.findViewById(R.id.timeBooking);
        firstNameBooking=view.findViewById(R.id.firstNameBooking);
        lastNameBooking=view.findViewById(R.id.lastNameBooking);
        birthDayBooking=view.findViewById(R.id.birthDayBooking);
        phoneNumberBooking=view.findViewById(R.id.phoneNumberBooking);
        emailBooking=view.findViewById(R.id.emailBooking);
//init fields
        timeBooking.setText(patient.getTimeOfBooking());
        firstNameBooking.setText(patient.getName());
        lastNameBooking.setText(patient.getLastName());
        birthDayBooking.setText(patient.getDateOfBirth());
        phoneNumberBooking.setText(patient.getPhoneNumber());
        emailBooking.setText(patient.getEmail());

        //** install spinner Cause Consultation
        spinCause=view.findViewById(R.id.spinnerCauseConsultation);
        spinCause.setOnItemSelectedListener(this);
        ArrayAdapter aa = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,causeOfConsultation);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinCause.setAdapter(aa);
        //end spinner

    }

    private void singleRowCalendarCreation() {

        Log.i("LOG", "DoctirCalendarFragment OnViewCreated");
        ma.widget.setOnDateChangedListener(this);
        ma.widget.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
        final LocalDate instance = LocalDate.now();
        ma.widget.setSelectedDate(instance);
        final LocalDate min = LocalDate.of(instance.getYear(), Month.JANUARY, 1);
        final LocalDate max = LocalDate.of(instance.getYear(), Month.DECEMBER, 31);
        ma.widget.state().edit().setMinimumDate(min).setMaximumDate(max).commit();
        ma.widget.addDecorators(
                new MySelectorDecorator(getActivity()),
                new HighlightWeekendsDecorator(),
                oneDayDecorator
        );
        ma.widget.state().edit()
                .setCalendarDisplayMode(CalendarMode.WEEKS)
                .commit();
        Log.i("LOG", "current day is: " + ma.widget.getCurrentDate() + "SelectedDate :" +
                ma.widget.getSelectedDate());

    }

    private int[] getDoctorScheduleButtonsState(View view, String selectedDay) {
        doctorScheduleManager = new DoctorScheduleManager(view);
        doctor = new Doctor();
        dayFragment = new DayFragment();
        prefStr = selectedDay + "Pref";
        pref = view.getContext().getSharedPreferences(prefStr, 0);

        return  dayFragment.getButtonsState(pref, doctorScheduleButtonState);
    }

    private void initHashMapWorkHours() {

        hashMapWorkingHours.put(0, "8:00am");
        hashMapWorkingHours.put(1, "9:00am");
        hashMapWorkingHours.put(2, "10:00am");
        hashMapWorkingHours.put(3, "11:00am");
        hashMapWorkingHours.put(4, "12:00am");
        hashMapWorkingHours.put(5, "1:00pm");
        hashMapWorkingHours.put(6, "2:00pm");
        hashMapWorkingHours.put(7, "3:00pm");
        hashMapWorkingHours.put(8, "4:00pm");
        hashMapWorkingHours.put(9, "5:00pm");
        hashMapWorkingHours.put(10, "6:00pm");
        hashMapWorkingHours.put(11, "7:00pm");
        hashMapWorkingHours.put(12, "8:00pm");
        hashMapWorkingHours.put(13, "9:00pm");
    }


    private void addTimeButtonToScrollView(View view, int buttonState[]) {
        LinearLayout linearLayout = view.findViewById(R.id.linearLayoutFreeTime);
        Log.i("LOG","linearLayout.getChildCount()"+linearLayout.getChildCount());
        linearLayout.removeAllViews();
        Log.i("LOG","linearLayout.getChildCount()"+linearLayout.getChildCount());
        linearLayout.setPadding(2, 2, 2, 2);
        for (int i = 0; i < doctorScheduleButtonState.length; i++) {


            Button btnTime = new Button(getContext());
            if (buttonState[i] == 0) {
                String text = hashMapWorkingHours.get(i);
                btnTime.setText(text);
                Log.i("LOG", "text:" + text);
                Log.i("LOG", "hashMapWorkingHours=" + hashMapWorkingHours.get(i));
                Log.i("LOG", "btnTime=" + btnTime.getText());

            } else {
                btnTime.setText(hashMapWorkingHours.get(i));
                btnTime.setEnabled(false);

            }
            btnTime.setId(i);
            linearLayout.addView(btnTime);
            setListenersToTimeButtons( linearLayout);
        }
    }

    private void setListenersToTimeButtons(LinearLayout linearLayout) {
       //here i need to add busy time to buttonStateArray in preferences
        //and to feel field time
        for(int i=0;i<linearLayout.getChildCount();i++){
            linearLayout.getChildAt(i).setOnClickListener((view -> {
                onTimeButtonsClick(view);
            }));
        }
    }

    private void onTimeButtonsClick(View view) {
        Button btnTime=(Button)view;
        Log.i("LOG","BUTTON id: "+btnTime.getId()+"  BUTTON Text: "+btnTime.getText());
        if (patient != null) {
            timeBooking.setText(btnTime.getText().toString());
            patient.setTimeOfBooking(btnTime.getText().toString());
        } else {
            Log.i("LOG", "patioent null");
        }
    }


    private void onAccountBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new PatientAccountFragment())
                .addToBackStack("PatietnAccountFragment")
                .commit();
    }

    private void onHealthBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new HealthCheckFragment())
                .addToBackStack("HealthCheckFragment")
                .commit();
    }

    private void onInboxBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new PatientInboxFragment())
                .addToBackStack("PatientInboxFragment")
                .commit();
    }


    private void onSearchBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new SearchFragment())
                .addToBackStack("SearchFragment")
                .commit();
    }

    private void onAppointmentBtn(View v) {

        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new PatientAppointmentFragment())
                .addToBackStack("PatientAppointmentFragment")
                .commit();
    }

    private void onBackButtonClick() {
        getParentFragmentManager().popBackStack();
    }


    @Override
    public void onUpdateSchedule(String weekDay, int[] buttonsState) {
        doctorScheduleManager.onUpdateSchedule(getContext(), weekDay, buttonsState);
        Log.i("schedule ", " PatientBookingFragment buttonState" + buttonsState);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onDateSelected( MaterialCalendarView widget, CalendarDay date, boolean selected) {

        getCurrentDay(ma.widget.getSelectedDate().getDate().toString());
        getDoctorScheduleButtonsState(getView(), getCurrentDay(ma.widget.getSelectedDate().getDate().toString()));
        addTimeButtonToScrollView(getView(), doctorScheduleButtonState);

    }

    String getCurrentDay(String tempDate) {

        //geting Day Name by selected date in Calendar
        LocalDate date1 = LocalDate.parse(tempDate);
        DayOfWeek day = date1.getDayOfWeek();

        String strDay = day.toString();
        //make all symbol to lower case
        String lowDay = strDay.toLowerCase();
        //then
        //make the first symbol to Upper case
        StringBuilder sb = new StringBuilder(lowDay);
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));

        return sb.toString();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}