package com.webmaster.doctoeasy.Patient;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.webmaster.doctoeasy.Appointment.Appointment;
import com.webmaster.doctoeasy.Appointment.AppointmentAdapter;
import com.webmaster.doctoeasy.Constants;
import com.webmaster.doctoeasy.Healthcheck.HealthCheckFragment;
import com.webmaster.doctoeasy.Main.MainActivity;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.Search.SearchFragment;
import com.webmaster.doctoeasy.client.FakeClient;
import com.webmaster.doctoeasy.decorators.HighlightWeekendsDecorator;
import com.webmaster.doctoeasy.decorators.MySelectorDecorator;
import com.webmaster.doctoeasy.decorators.OneDayDecorator;

import org.threeten.bp.LocalDate;
import org.threeten.bp.Month;

import java.util.ArrayList;

import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PatientAppointmentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PatientAppointmentFragment extends Fragment implements OnDateSelectedListener {

    private final OneDayDecorator oneDayDecorator = new OneDayDecorator();
    MainActivity ma;
    SharedPreferences mPref;
    String prefName;
    Bundle bundle;
    ArrayList<Patient> patientBookingArrayList;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PatientAppointmentFragment() {

    }

    public PatientAppointmentFragment(OnDateSelectedListener listener) {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DoctorCalendarFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PatientAppointmentFragment newInstance(String param1, String param2) {
        PatientAppointmentFragment fragment = new PatientAppointmentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.patient_appointment_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        prefName = "ArrayListOfBookingPatient";
        mPref = getContext().getSharedPreferences(prefName, 0); //geting MondayPref
        ma = (MainActivity) getActivity();
        ButterKnife.bind(getActivity());

//work tools
//clearArrayBookingPatientList();  // use for clear the arraY LIST


        /*
        /* SingleRowCalendarCreated
        */
        singleRowCalendarCreated();

        /*
        /*LIST VIEW CREATED
        */
        listViewCreated(view);


        /*
        /* MainMenu Listeners
        */
        view.findViewById(R.id.btnBackSignUp).setOnClickListener((v -> {
            onBackButtonClick(v);
        }));


        view.findViewById(R.id.btnPatientAccount).setOnClickListener((v -> {
            onAccountBtn(v);
        }));

        view.findViewById(R.id.btnPatientSearch).setOnClickListener((v) -> {
            onSearchBtn(v);
        });

        view.findViewById(R.id.btnPatientAppointment).setOnClickListener((v) -> {
            onAppointmentBtn(v);
        });

        view.findViewById(R.id.btnPatientInbox).setOnClickListener((v) -> {
            onInboxBtn(v);
        });

        view.findViewById(R.id.btnPatientHealthCheck).setOnClickListener((v) -> {
            onHealthBtn(v);
        });

    }

    private void clearArrayBookingPatientList() {
        ArrayList<Patient> patientBookingArrayList;
        patientBookingArrayList = FakeClient.getClient(getContext()).extractPatientArrayListFromPref(mPref, prefName);
        if (patientBookingArrayList != null) {
            patientBookingArrayList = null;
            FakeClient.getClient(getContext()).saveArrayListToPreference(patientBookingArrayList, mPref, prefName);
        }
    }


    private void singleRowCalendarCreated() {

        ma.widget.setOnDateChangedListener(this);
        ma.widget.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
        final LocalDate instance = LocalDate.now();
        ma.widget.setSelectedDate(instance);
        final LocalDate min = LocalDate.of(instance.getYear(), Month.JANUARY, 1);
        final LocalDate max = LocalDate.of(instance.getYear(), Month.DECEMBER, 31);
        ma.widget.state().edit().setMinimumDate(min).setMaximumDate(max).commit();
        ma.widget.addDecorators(
                new MySelectorDecorator(getActivity()),
                new HighlightWeekendsDecorator(),
                oneDayDecorator
        );
        ma.widget.state().edit()
                .setCalendarDisplayMode(CalendarMode.WEEKS)
                .commit();
    }

    private void listViewCreated(View view) {

        ArrayList<Appointment> appointments = new ArrayList<>();
        String selectedDate = ma.widget.getSelectedDate().getDate().toString();
        patientBookingArrayList = FakeClient.getClient(getContext()).extractPatientArrayListFromPref(mPref, Constants.prefName);
        appointments.clear();
        if (patientBookingArrayList != null) {
            for (int i = 0; i < patientBookingArrayList.size(); i++) {
                if (selectedDate.equals(patientBookingArrayList.get(i).getDateOfBooking())) {
                    if(patientBookingArrayList.get(i).getStatus()==Patient.Status.CONFIRMED){

                        appointments.add(new Appointment(
                                getResources().getDrawable(R.drawable.img_clinica1),            //clinica
                                patientBookingArrayList.get(i).getTimeOfBooking(),              //time
                                patientBookingArrayList.get(i).getName(),                       //name
                                patientBookingArrayList.get(i).getLastName(),                   //last name
                                patientBookingArrayList.get(i).getStatus().toString(),                                                         //status
                                getResources().getDrawable(R.drawable.img_confirmed)));       //img status
                    }
                    if(patientBookingArrayList.get(i).getStatus()==Patient.Status.NEW){
                        Drawable statusPatient=getResources().getDrawable(R.drawable.img_new);
                        appointments.add(new Appointment(
                                getResources().getDrawable(R.drawable.img_clinica1),            //clinica
                                patientBookingArrayList.get(i).getTimeOfBooking(),              //time
                                patientBookingArrayList.get(i).getName(),                       //name
                                patientBookingArrayList.get(i).getLastName(),                   //last name
                                patientBookingArrayList.get(i).getStatus().toString(),                                                         //status
                                statusPatient ));       //img status
                    }
                    if(patientBookingArrayList.get(i).getStatus()==Patient.Status.CANCELED){

                        Drawable statusPatient=getResources().getDrawable(R.drawable.img_canceled);
                        appointments.add(new Appointment(
                                getResources().getDrawable(R.drawable.img_clinica1),            //clinica
                                patientBookingArrayList.get(i).getTimeOfBooking(),              //time
                                patientBookingArrayList.get(i).getName(),                       //name
                                patientBookingArrayList.get(i).getLastName(),                   //last name
                                patientBookingArrayList.get(i).getStatus().toString(),                                                         //status
                                statusPatient ));       //img status
                    }
                }
            }
        }

        ListView listView = view.findViewById(R.id.list_order);
        AppointmentAdapter adapter = new AppointmentAdapter(getActivity(), R.layout.appointments_items, appointments);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }


    private int getSelectedPatient(View view) {
//        TextView textViewFirstName;
//        String arraylistFirstName;
//        for(int i=0;i<patientBookingArrayList.size();i++){
//
//            textViewFirstName=view.findViewById(R.id.textViewFirstName);
//             arraylistFirstName=patientBookingArrayList.get(i).getName();
//
//
//            Log.i("LOG", "firstName: "+textViewFirstName.getText());
//            Log.i("LOG", "arraylistFirstName "+arraylistFirstName);
//
//            if(textViewFirstName.getText().equals(arraylistFirstName)){
//            return i;
//        }
//
//    }
    return -1;
    }


    private Bundle saveCurrentPatientInBundle(Patient patient) {

//        Gson gson = new Gson();
//        String strJson = gson.toJson(patient);         //convert arrayListPatient to Json
//        Log.i("LOG", "patient: "+patient);
//        bundle.putString("PatientInfo",strJson);
return bundle;
    }

    private void onAccountBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new PatientAccountFragment())
                .addToBackStack("PatientAccountFragment")
                .commit();
    }

    private void onHealthBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new HealthCheckFragment())
                .addToBackStack("HealthCheckFragment")
                .commit();
    }

    private void onInboxBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new PatientInboxFragment())
                .addToBackStack("PatientInboxFragment")
                .commit();
    }


    private void onSearchBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new SearchFragment())
                .addToBackStack("SearchFragment")
                .commit();
    }

    private void onAppointmentBtn(View v) {

        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new PatientAppointmentFragment())
                .addToBackStack("PatientAppointmentFragment")
                .commit();
    }

    private void onBackButtonClick(View v) {
        getParentFragmentManager().popBackStack();
    }


    @Override
    public void onDateSelected( MaterialCalendarView widget,  CalendarDay date, boolean selected) {
        if( getView()!=null){
            listViewCreated(getView());
            Log.i("LOG", "onDateSelected: +++");

        }

    }


}