package com.webmaster.doctoeasy.Patient;

public class Patient {

    //personal info
    private int id;
    private String name;
    private String lastName;
    private Gender gender;
    private String dateOfBirth;
    private String phoneNumber;
    private String email;
    private String photoPath;

    //booking info
    private String login;
    private String password;
    private boolean registered;
    public Status status;   //appointment status

    public String getLoginOfDoctorsBooking() {
        return loginOfDoctorsBooking;
    }

    public void setLoginOfDoctorsBooking(String loginOfDoctorsBooking) {
        this.loginOfDoctorsBooking = loginOfDoctorsBooking;
    }

    private String loginOfDoctorsBooking; //when patient booking to doctors then we save doctors login there
    private String dateOfBooking;
    private String timeOfBooking;
    private String causeBooking;

    public String getPhotoPath() {
        return photoPath;
    }
    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }



    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public enum Gender{
        FEMALE,
        MALE
    }
    public enum Status {   //appointment status
        CONFIRMED,
        CANCELED,
        NEW
    }



    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


    public  Patient(String name,String lastName,Gender gender,String dateOfBirth,String phoneNumber,String email,
                    String login,String password){

    this.name= name;
    this.lastName=lastName;
    this.gender=gender;
    this.dateOfBirth=dateOfBirth;
    this.phoneNumber=phoneNumber;
    this.email=email;
    this.login=login;
    this.password=password;

         dateOfBooking="";
         timeOfBooking="";
         causeBooking="";
         registered=true;
         status=Status.NEW;

    }

public Patient() {
    this.name= "name";
    this.lastName="lastName";
    this.gender=Gender.MALE;
    this.dateOfBirth="dateOfBirth";
    this.phoneNumber="phoneNumber";
    this.email="email";
    this.login="login";
    this.password="password";
    dateOfBooking="";
    timeOfBooking="";
    causeBooking="";
    registered=true;
    status=Status.NEW;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDateOfBooking() {
        return dateOfBooking;
    }

    public void setDateOfBooking(String dateOfBooking) {
        this.dateOfBooking = dateOfBooking;
    }

    public String getTimeOfBooking() {
        return timeOfBooking;
    }

    public void setTimeOfBooking(String timeOfBooking) {
        this.timeOfBooking = timeOfBooking;
    }

    public String getCauseBooking() {
        return causeBooking;
    }

    public void setCauseBooking(String causeBooking) {
        this.causeBooking = causeBooking;
    }


}
