package com.webmaster.doctoeasy.Patient;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import com.webmaster.doctoeasy.Login.LoginFragment;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.client.FakeClient;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class PatientSignUpFragment extends Fragment {

    TextView name;
    TextView lastName;
    TextView phoneNumber;
    TextView email;
    TextView login;
    TextView password;
    TextView dateOfBirth;
    RadioButton female;
    RadioButton male;
    Patient.Gender gender = Patient.Gender.MALE;
    final Calendar myCalendar = Calendar.getInstance();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_patient_sign_up, container, false);
        return v;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        name = view.findViewById(R.id.etSignUpPatientName);
        lastName = view.findViewById(R.id.etSignUpPatietnLastName);
        phoneNumber = view.findViewById(R.id.etPhoneNumber);
        email = view.findViewById(R.id.etSignUpPatientEmail);
        login = view.findViewById(R.id.etSignUpPatientLogin);
        password = view.findViewById(R.id.etSignUpPatientNewPassword);
        dateOfBirth = view.findViewById(R.id.etSignUpPatientDateOfBirth);
        female=view.findViewById(R.id.rbDoctorGenderMale);
         male=view.findViewById(R.id.rbDoctorGenderMale);

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, day);
                updateLabel();
            }

            private void updateLabel() {
                String myFormat = "MM.dd.yy";
                SimpleDateFormat dateFormat = new SimpleDateFormat(myFormat, Locale.US);
                dateOfBirth.setText(dateFormat.format(myCalendar.getTime()));
            }
        };


        dateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(), date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        view.findViewById(R.id.btn_SignUpPatient).setOnClickListener((v) -> {
            onSignUpPatientBtn(v);
        });

        view.findViewById(R.id.btnBackSignUp).setOnClickListener((v) -> {
            onArrowBackBtn(v);
        });

    }

    public void onArrowBackBtn(View view) {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    //обработчик события нажатия кнопки btnSignupPatient
    public void onSignUpPatientBtn(View view) {

        if (female.isChecked()) {
            gender = Patient.Gender.FEMALE;
        } else if (male.isChecked()) {
            gender = Patient.Gender.MALE;
        }

       String s_name =name.getText().toString();
       String s_lname =lastName.getText().toString();
       String s_phoneNumber =phoneNumber.getText().toString();
       String s_email =email.getText().toString();
       String s_login =login.getText().toString();
       String s_paww =password.getText().toString();
       String s_dateOfBirth =dateOfBirth.getText().toString();

        Patient patient = new Patient(
                s_name,
                s_lname,
                gender,
                s_dateOfBirth,
                s_phoneNumber,
                s_email,
                s_login,
                s_paww
        );

        FakeClient.getClient(getContext()).newPatientRegistration(getContext(),s_login,patient);
        Toast.makeText(getContext(), "Successful Registered Patient!", Toast.LENGTH_SHORT).show();
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new LoginFragment())
                .addToBackStack("LoginFragment")
                .commit();

    }

}