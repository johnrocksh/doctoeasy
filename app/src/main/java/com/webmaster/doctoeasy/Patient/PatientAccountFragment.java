package com.webmaster.doctoeasy.Patient;

import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.webmaster.doctoeasy.Constants;
import com.webmaster.doctoeasy.Healthcheck.HealthCheckFragment;
import com.webmaster.doctoeasy.Main.MainFragment;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.Search.SearchFragment;
import com.webmaster.doctoeasy.client.FakeClient;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class PatientAccountFragment extends Fragment {

    ImageView ivPatientPhoto;
    private static final int PICK_IMAGE = 100;
    Uri imageUri;
    static Patient patient;
     EditText firstName;
     EditText lastName;
     EditText email;
     EditText telephone;
     EditText login;
     EditText newPassword;
    EditText oldPassword;
    ImageView ivEyeShowOldPsw;
    ImageView ivEyeShowNewPsw;

    boolean fl_eye=false;
    boolean fl_eyeNew=false;;
    public PatientAccountFragment() {
        // Required empty public constructor
    }

    public static PatientAccountFragment newInstance(String param1, String param2) {
        PatientAccountFragment fragment = new PatientAccountFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_patient_accaunt, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
            ivEyeShowOldPsw=view.findViewById(R.id.ivEyeShowOldPsw);
            ivEyeShowNewPsw=view.findViewById(R.id.ivEyeShowNewPsw);

            firstName=view.findViewById(R.id.etFirstNameDoctor);
            lastName=view.findViewById(R.id.etSurnameDoctor);
            email=view.findViewById(R.id.etEmailDoctor);
            telephone=view.findViewById(R.id.etTelDoctor);
            login=view.findViewById(R.id.etLoginDoctor);
            newPassword=view.findViewById(R.id.etNewPassw);
         oldPassword=view.findViewById(R.id.etOldPsw);
        ivPatientPhoto = view.findViewById(R.id.ivDoctorPhoto);
        String patientLogin = getArguments().getString("login");
        String patientPassword = getArguments().getString("password");
        Log.i("LOG", "login: "+patientLogin);
        Log.i("LOG", "password: "+patientPassword);
        patient=FakeClient.getClient(getContext()).getPatient(getContext(),patientLogin);

        initPatientAccount(patient);
        view.findViewById(R.id.btnDoctorUploadPhoto).setOnClickListener((v -> {
            onClickUploadPhoto(v);
        }));

        view.findViewById(R.id.ivEyeShowOldPsw).setOnClickListener((v->{

            onBtnEyeOldPswClick(v);

        }));
        view.findViewById(R.id.ivEyeShowNewPsw).setOnClickListener((v->{

            onBtnEyeNewPaswClick(v);

        }));

        //меняем цвет фона кнопки в меню
        view.findViewById(R.id.btnPatientAccount).setBackgroundResource(R.drawable.bottom_menu_pressed_bg);

        view.findViewById(R.id.btnBackFragment).setOnClickListener((v -> {
            onBackButtonClick(v);
        }));

        view.findViewById(R.id.btnPatientSearch).setOnClickListener((v) -> {
            onSearchBtn(v);
        });

        view.findViewById(R.id.btnPatientAppointment).setOnClickListener((v) -> {
            onAppointmentBtn(v);
        });

        view.findViewById(R.id.btnPatientInbox).setOnClickListener((v) -> {
            onInboxBtn(v);
        });

        view.findViewById(R.id.btnPatientHealthCheck).setOnClickListener((v) -> {
            onHealthBtn(v);
        });

        view.findViewById(R.id.btnPatientSignOut).setOnClickListener((v -> {
            onBtnSignOutClick(v);
        }));

        view.findViewById(R.id.btnPatientUpdatePassword).setOnClickListener((v)->{

            onBtnChangePassword(v);

        });

    }



    private void onBtnEyeOldPswClick(View v) {
        if(fl_eye==false){

            ivEyeShowOldPsw.setImageResource(R.drawable.eye);
            oldPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            fl_eye=true;
        }
        else
        if(fl_eye==true){

            ivEyeShowOldPsw.setImageResource(R.drawable.eye_close);
            oldPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            fl_eye=false;
        }

    }

    private void onBtnEyeNewPaswClick(View v) {
        if(fl_eyeNew==false){

            ivEyeShowNewPsw.setImageResource(R.drawable.eye);
            newPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            fl_eyeNew=true;
        }
        else
        if(fl_eyeNew==true){

            ivEyeShowNewPsw.setImageResource(R.drawable.eye_close);
            newPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            fl_eyeNew=false;
        }


    }

    private void onBtnChangePassword(View v) {
        //взять логин пациента вытащить его пароль
        //сравнить старый пароль введенный только что с паролем пациента если все ок то меняем его на новый пароль

        if((newPassword.getText().length()==0)||(oldPassword.getText().length()==0)){
            Toast.makeText(getContext(), "Please feel the fields and try again!", Toast.LENGTH_SHORT).show();
            return;
        }
        Log.i("LOG", "patient pasw: "+patient.getPassword());
        Log.i("LOG", "new pasw: "+newPassword.getText().toString());
        Log.i("LOG", "old pasw: "+oldPassword.getText().toString());


        if(patient.getPassword().equals(oldPassword.getText().toString())){
                FakeClient.getClient(getContext()).changePatientPassword(getContext(),login.getText().toString(),newPassword.getText().toString());
             Toast.makeText(getContext(),"Password was changed successful!",Toast.LENGTH_SHORT);
        }
                else {
                    Toast.makeText(getContext(),"Incorrect old password!",Toast.LENGTH_SHORT);
                }
    }



    private void initPatientAccount(Patient patient) {

        firstName.setText(patient.getName());
        lastName.setText(patient.getLastName());
        telephone.setText(patient.getPhoneNumber());
        email.setText(patient.getEmail());
        login.setText(patient.getLogin());
        setPhoto(patient.getPhotoPath());

    }

    private void setPhoto(String photoPath) {

       if(photoPath!=null){
           File imgFile = new  File(patient.getPhotoPath());
           if(imgFile.exists()){

               Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
               ivPatientPhoto.setImageBitmap(myBitmap);
       }
        }
    }

    private void onClickUploadPhoto(View v) {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imageUri = data.getData();
            ivPatientPhoto.setImageURI(imageUri);
            String fileName = getFileName(imageUri);
            String realPath=getRealPathFromURI(getContext(),imageUri);
            savePhoto(imageUri,realPath, fileName);
        }
    }

    private String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            Log.e("LOG", "getRealPathFromURI Exception : " + e.toString());
            return "";
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }



    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }



    @RequiresApi(api = Build.VERSION_CODES.O)
    private void savePhoto(Uri uri,String filePath, String fileName) {
        Log.i("LOG", "savePhoto: "+filePath+fileName);
        //first create folder then..
        File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+File.separator+ Constants.dirPatientsPhoto);
        Log.i("LOG", "newFolder: "+folder.getPath());

        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        if (success) {

            try {
               String patientPhoto=filePath;
               Context context=getContext();
                  copyFile(context, patientPhoto,folder);
            } catch (IOException e) {
                e.printStackTrace();
                Log.i("LOG", "IOException: "+e);
            }

        } else {
            Toast.makeText(getContext(), "DoctorEasy can't create the Folder!", Toast.LENGTH_SHORT).show();
        }
        }

    //////////////
    //copyFile
    /////////////
    @RequiresApi(api = Build.VERSION_CODES.O)
    private static void copyFile(Context context,String patientPhoto, File newFolder) throws IOException {
        //from
        File originalFile=new File(patientPhoto);
        Path pathOut = (Path)Paths.get(originalFile.getPath());
        //to
        Path pathIn = Paths.get(newFolder.getPath()+"/"+patient.getName()+patient.getLogin()+".jpg");
       // !!!//call the update method for patient in Fake client and save photo path
        FakeClient.getClient(context).savePhotoPathInPatient(context,patient,pathIn);
        Files.copy(pathOut, pathIn, StandardCopyOption.REPLACE_EXISTING);
    }


    private void onBtnSignOutClick(View v) {

    getParentFragmentManager().beginTransaction()
        .replace(R.id.fragments_container,new MainFragment())
        .commit();
    }

    private void onBackButtonClick(View v) {
        FragmentManager fm = getParentFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
        } else {
            Log.i("Log", "nothing on backstack, calling super");
        }
    }

    private void onHealthBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new HealthCheckFragment())
                .addToBackStack("HealthCheckFragment")
                .commit();
    }

    private void onInboxBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientInboxFragment())
                .addToBackStack("PatientInboxFragment")
                .commit();
    }


    private void onSearchBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new SearchFragment())
                .addToBackStack("SearchFragment")
                .commit();
    }

    private void onAppointmentBtn(View v) {

        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientAppointmentFragment())
                .addToBackStack("PatientAppointmentFragment")
                .commit();
    }
}