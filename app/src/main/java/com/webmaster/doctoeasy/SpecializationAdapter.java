package com.webmaster.doctoeasy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

public class SpecializationAdapter extends ArrayAdapter<Specialization> {

    private LayoutInflater infl;
    private int resource;
    private List<Specialization> specializations;

    public SpecializationAdapter(@NonNull Context context, int resource, List<Specialization>specializations){
        super(context,resource,specializations);

        this.infl=LayoutInflater.from(context);

        this.resource=resource;
        this.specializations=specializations;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
      ViewHolder viewHolder;

        if(convertView==null)
        {
            convertView=infl.inflate(this.resource,parent,false);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        }
        else
            viewHolder=(ViewHolder)convertView.getTag();

        final Specialization specialization=specializations.get(position);
        viewHolder.textViewSpecialization.setText(specialization.getItem());
        viewHolder.imageViewSelected.setImageDrawable(specialization.getSelected());

        //

        return convertView;
    }



    private class ViewHolder{

        final TextView textViewSpecialization;
        final ImageView imageViewSelected;

        ViewHolder (View view){
            textViewSpecialization=view.findViewById(R.id.textViewSpetialisation);
            imageViewSelected=view.findViewById(R.id.imageViewSelected);
        }
    }
}
