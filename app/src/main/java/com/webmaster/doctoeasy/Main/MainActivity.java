package com.webmaster.doctoeasy.Main;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.SupportMapFragment;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.webmaster.doctoeasy.Doctor.DoctorCalendarFragment;
import com.webmaster.doctoeasy.OnBackPressed;
import com.webmaster.doctoeasy.Patient.PatientBookingFragment;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.decorators.OneDayDecorator;

import butterknife.BindView;

public class MainActivity extends AppCompatActivity  {
  public   @BindView(R.id.myCalendarView) MaterialCalendarView widget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_root);


        Fragment fragment = new MainFragment(); //здесь я цепляю первый гланвй фрагмент
        //Fragment fragment = new DoctorCalendarFragment(); //здесь я цепляю первый гланвй фрагмент
        getSupportFragmentManager()
            .beginTransaction()
            .add(R.id.fragments_container, fragment)
            .commit();


    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragments_container);
        if(f instanceof OnBackPressed)
            // do something with f
            ((OnBackPressed) f).onBackPressed();

        //super.onBackPressed();
    }


}
