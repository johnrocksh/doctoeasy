package com.webmaster.doctoeasy.Main;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.webmaster.doctoeasy.Login.LoginFragment;
import com.webmaster.doctoeasy.Login.SignupFragment;
import com.webmaster.doctoeasy.R;

public class MainFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View root = inflater.inflate(R.layout.fragment_main, container, false);

        root.findViewById(R.id.btnMainLogin).setOnTouchListener((v, e) -> {
            return onLoginTouch(v);
        });

        root.findViewById(R.id.btnMainSignup).setOnClickListener((v) -> {
            onSignupBtn(v);
        });

//        root.findViewById(R.id.btnPatientAccount).setOnClickListener((v->{
//            onSignupBtn(v);
//    }));



        return root;
    }



    public boolean onLoginTouch(View view) {
        Log.d("DEasy", "onLoginBtn");
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new LoginFragment())
                .addToBackStack("LoginFragment")
                .commit();
        Log.d("DEasy", "LoginFragment ready");
        return true;
    }

    public void onSignupBtn(View view) {
        Log.d("DEasy", "onSignupBtn");
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new SignupFragment())
                .addToBackStack("SignupFragment")
                .commit();
        Log.d("DEasy", "SignupFragment ready");
    }
}