package com.webmaster.doctoeasy.Main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.webmaster.doctoeasy.Healthcheck.HealthCheckFragment;
import com.webmaster.doctoeasy.OnBackPressed;
import com.webmaster.doctoeasy.Patient.PatientAppointmentFragment;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.Search.SearchByDateFragment;
import com.webmaster.doctoeasy.Search.SearchByLocationFragment;
import com.webmaster.doctoeasy.Search.SearchBySpecialistFragment;
import com.webmaster.doctoeasy.Search.SearchResultFragment;

public class MainSearchFragment extends Fragment implements OnBackPressed {

      @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.activity_main_search, container, false);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


        bottomMenuSetListeners(view);
        searchFieldsSetListeners(view);

    }

  void bottomMenuSetListeners(View view){
      view.findViewById(R.id.btnPatientSearch).setOnClickListener((v) -> {
          onSearchBtn(v);
      });

      view.findViewById(R.id.btnPatientAppointment).setOnClickListener((v) -> {
          onAppointmentBtn(v);
      });

      view.findViewById(R.id.btnPatientInbox).setOnClickListener((v) -> {
          onInboxBtn(v);
      });

      view.findViewById(R.id.btnPatientHealthCheck).setOnClickListener((v) -> {
          onHealthBtn(v);
      });

      view.findViewById(R.id.btnPatientAccount).setOnClickListener((v) -> {
          onLoginBtn(v);
      });

    }

  void  searchFieldsSetListeners(View view){
      view.findViewById(R.id.editTextArea).setOnClickListener((v) -> {
          editTextAreaOnClick(v);
      });

      view.findViewById(R.id.editTextDate).setOnClickListener((v) -> {
          editTextDateOnClick(v);
      });

      view.findViewById(R.id.editTextSpecialist).setOnClickListener((v) -> {
          editTextSpecialistOnClick(v);
      });

      view.findViewById(R.id.btnNextArrow).setOnClickListener((v) -> {
          btnNextOnClick(v);
      });
  }


    private void onLoginBtn(View v) {
//        Intent intent =new Intent(getActivity(), PatientAccauntActivity_old.class);
//        startActivity(intent);
    }

    private void onHealthBtn(View v) {
       getParentFragmentManager().beginTransaction()
               .replace(R.id.fragments_container,new HealthCheckFragment())
               .addToBackStack("HealthCheckFragment")
               .commit();
    }

    private void onInboxBtn(View v) {
//        Intent intent =new Intent(getActivity(), PatientInboxActivity_old.class);
//        startActivity(intent);
    }

    private void onAppointmentBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new PatientAppointmentFragment())
                .addToBackStack("PatientAppointmentFragment")
                .commit();
    }

    private void onSearchBtn(View v) {

    }

    public void backArrowClick(View view) {


    }

    public void editTextSpecialistOnClick(View view) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new SearchBySpecialistFragment())
                .addToBackStack("SearchBySpecialistFragment")
                .commit();
    }

    public void editTextAreaOnClick(View view) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new SearchByLocationFragment())
                .addToBackStack("SearchByLocationFragment")
                .commit();
    }

    public void editTextDateOnClick(View view) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new SearchByDateFragment())
                .addToBackStack("SearchByDateFragment")
                .commit();
    }


    public void btnNextOnClick(View view) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, new SearchResultFragment())
                .addToBackStack("SearchResultFragment")
                .commit();
    }


    @Override
    public void onBackPressed() {

            getActivity().getSupportFragmentManager().popBackStack();
    }
}
