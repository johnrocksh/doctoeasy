package com.webmaster.doctoeasy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

public class SpecialistAdapter extends ArrayAdapter<Specialist>  {

    private LayoutInflater infl;
    private int resource;
    private List<Specialist> specialists;

    public SpecialistAdapter(@NonNull Context context, int resource, List<Specialist> specialists) {

        super(context, resource, specialists);
        this.infl = LayoutInflater.from(context);
        this.resource = resource;
        this.specialists = specialists;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {

            convertView = infl.inflate(this.resource, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else

            viewHolder = (ViewHolder) convertView.getTag();
        final Specialist specialist = specialists.get(position);

        viewHolder.textViewName.setText(specialist.getName());
        viewHolder.textViewspecialisation.setText(specialist.getSpecialisation());
        viewHolder.imageViewPhoto.setImageDrawable(specialist.getPhoto());
        viewHolder.imageViewStar1.setImageDrawable(specialist.getStarRating()[0]);
        viewHolder.imageViewStar2.setImageDrawable(specialist.getStarRating()[1]);
        viewHolder.imageViewStar3.setImageDrawable(specialist.getStarRating()[2]);
        viewHolder.imageViewStar4.setImageDrawable(specialist.getStarRating()[3]);
        viewHolder.imageViewStar5.setImageDrawable(specialist.getStarRating()[4]);






        return convertView;

    }

    private class ViewHolder {

        final ImageView imageViewPhoto;
        final TextView textViewName;
        final TextView textViewspecialisation;
        final ImageView imageViewStar1;
        final ImageView imageViewStar2;
        final ImageView imageViewStar3;
        final ImageView imageViewStar4;
        final ImageView imageViewStar5;
        final ImageView imageViewBtnBook;


        ViewHolder(View view) {

            imageViewPhoto = view.findViewById(R.id.imageViewSpecialistPhoto);
            textViewName = view.findViewById(R.id.textViewSpecialistName);
            textViewspecialisation = view.findViewById(R.id.textViewSpetialisation);
            imageViewStar1 = view.findViewById(R.id.imageViewStar1);
            imageViewStar2 = view.findViewById(R.id.imageViewStar2);
            imageViewStar3 = view.findViewById(R.id.imageViewStar3);
            imageViewStar4 = view.findViewById(R.id.imageViewStar4);
            imageViewStar5 = view.findViewById(R.id.imageViewStar5);
            imageViewBtnBook=view.findViewById(R.id.btnBook);


        }
    }


}
