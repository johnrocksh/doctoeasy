package com.webmaster.doctoeasy.Login;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.webmaster.doctoeasy.Patient.Patient;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.client.FakeClient;
import com.webmaster.doctoeasy.client._SendMailInBackground.GMailSender;

import java.util.Random;

public class RecoveryPassword extends Fragment {

    String GMail = "shevchenkovania@gmail.com"; //replace with you GMail
    String GMailPass = "Salvation777###"; // replace with you GMail Password

    Handler h;
    EditText etlogin;
    EditText etSecretCode;
    String secretCode;
    Button btnOk;
    public RecoveryPassword() {
        // Required empty public constructor
    }
    public static RecoveryPassword newInstance(String param1, String param2) {
        RecoveryPassword fragment = new RecoveryPassword();
        return fragment;
    }

//короче нужно все в отдельный поток упоковать иначе выкидывает

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recovery_pasword, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btnOk).setOnClickListener((v)->{
            onBtnOk(v);
        });
        etSecretCode=view.findViewById(R.id.etSecretCode);
        etlogin=view.findViewById(R.id.etEnterLogin);
               view.findViewById(R.id.btnSendSecretCode).setOnClickListener((v)->{
            onSendCodeClick(v);

        });

    }

    private void onBtnOk(View v) {
        if(etSecretCode.getText().toString().equals(secretCode)){
            Toast.makeText(getContext(), "Sectret Code Correct!!!", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(getContext(),"incorect secret Code",Toast.LENGTH_SHORT).show();
        }
    }

    private void sendEmail(final String to, final String subject, final String message) {

new Thread(new Runnable() {
    @Override
    public void run() {
        /////////
        try {
            GMailSender sender = new GMailSender(GMail, GMailPass);
            sender.sendMail(subject,
                    message,
                    GMail,
                    to);
            Log.w("sendEmail","Email successfully sent!");

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getContext(), "Email successfully sent!", Toast.LENGTH_LONG).show();

                }
            });

        } catch (final Exception e) {
            Log.e("sendEmail", e.getMessage(), e);

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getContext(), "Email not sent. \n\n Error: " + e.getMessage().toString(), Toast.LENGTH_LONG).show();
                }
            });

        }
        ///////////

    }
}).start();


    }

    private String getSecretCode() {
        final Random numRandom = new Random();
        String code="";
        code=String.valueOf(numRandom.nextInt(1000));
        return code;
    }


    private void onSendCodeClick(View v) {

        Patient p;
        String email="";
        p= FakeClient.getClient(getContext()).getPatient(getContext(),etlogin.getText().toString());

        if (p!=null){
            email="ivanshevchenko@mail.ru";//p.getEmail();
            Log.i("LOG", "email: "+email);
        }
        else{
            Toast.makeText(getContext(), "User with this login not found!", Toast.LENGTH_SHORT).show();
        }

        String str_to = "ivanshevchenko@mail.ru";
        secretCode = getSecretCode();
        String str_subject =p.getLogin();

        sendEmail(str_to, str_subject, secretCode);


}}