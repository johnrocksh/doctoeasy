package com.webmaster.doctoeasy.Login;

import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.webmaster.doctoeasy.Doctor.DoctorAccountFragment;
import com.webmaster.doctoeasy.Healthcheck.HealthCheckFragment;
import com.webmaster.doctoeasy.OnBackPressed;
import com.webmaster.doctoeasy.Patient.PatientAccountFragment;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.Search.SearchFragment;
import com.webmaster.doctoeasy.client.FakeClient;

public class LoginFragment extends Fragment implements OnBackPressed {

 ImageView imageViewOrangeCube;
 TextView  textViewPageName;
 TextView  textViewForgotPassword;
 EditText  login;
 EditText  password;
 TextView tvForgotPsw;


 public Layout layout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

        View v = inflater.inflate(R.layout.fragment_login, container, false);
       return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        imageViewOrangeCube = view.findViewById(R.id.btnLoginRememberMe);
        textViewPageName = view.findViewById(R.id.textViewPageName);


        textViewForgotPassword = view.findViewById(R.id.textViewForgotPassword);

        login = view.findViewById(R.id.etEmailDoctor);
        password = view.findViewById(R.id.editTextPassword);

        view.findViewById(R.id.tvForgotPsw).setOnClickListener((v)->{
            onForgotPswClick(v);
        });

        view.findViewById(R.id.btnLoginRememberMe).setOnClickListener((v) -> {
            onCheckboxBtn(v);
        });

        view.findViewById(R.id.btnLoginArrowNext).setOnClickListener((v) -> {
        onArrowNextBtn(v);
        });

        view.findViewById(R.id.btnBackSignUp).setOnClickListener((v) -> {
        onArrowBackBtn(v);
        });
/////////////обработка меню

      view.findViewById(R.id.btnLogin).setOnClickListener((v->{
          onBtnLoginClick(v);
      }));

      view.findViewById(R.id.btnHealth).setOnClickListener((v->{
          onBtnHealthClick(v);
      }));

      view.findViewById(R.id.btnInbox).setOnClickListener((v->{
          onBtnInbox(v);
      }));

      view.findViewById(R.id.btnAppointment).setOnClickListener((v->{
          onBtnAppointmentClick(v);
      }));

      view.findViewById(R.id.btnSearch).setOnClickListener((v->{
          onBtnSearchClick(v);
      }));
/////////////

        }

    private void onForgotPswClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new RecoveryPassword())
                .addToBackStack("PasswordRecovery")
                .commit();
    }

    private void onBtnSearchClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new SearchFragment())
                .addToBackStack("SearchFragment")
                .commit();

    }

    private void onBtnAppointmentClick(View v) {
        Toast toast = Toast.makeText(getContext(),
                "Please enter to your account",Toast.LENGTH_SHORT);
        toast.show();
    }

    private void onBtnInbox(View v) {

        Toast toast = Toast.makeText(getContext(),
                "Please enter to your account", Toast.LENGTH_SHORT);
        toast.show();

    }

    private void onBtnHealthClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new HealthCheckFragment())
                .addToBackStack("HealthCheckFragment")
                .commit();
    }

    private void onBtnLoginClick(View v) {
        //ничего не происходит -- остаемся на месте
    }

    public void onArrowBackBtn(View view) {
        onBackPressed();
    }

    public void onCheckboxBtn(View view) {
        if(view.getVisibility() == View.VISIBLE)
            view.setVisibility(View.INVISIBLE);
        else
           view.setVisibility(View.VISIBLE);
    }

   //обработчик события нажатия кнопки перед после ввода логина и пароля
    public void onArrowNextBtn(View view) {

        Login loginResult= FakeClient.getClient(getContext()).login(getContext(),login.getText().toString(),password.getText().toString());

          if(loginResult.doctor!=null){

              Bundle bundle= new Bundle();
              bundle.putString("password",loginResult.doctor.getPassword());
              bundle.putString("login",loginResult.doctor.getLogin());
              Fragment fragment=new DoctorAccountFragment();
              fragment.setArguments(bundle);

                             getParentFragmentManager().beginTransaction()
                             .replace(R.id.fragments_container,fragment)
                             .addToBackStack("DoctorAccountFragment")
                              .commit();

          }
          else if(loginResult.patient!=null)
              {
                  Bundle bundle = new Bundle();
                   bundle.putString("password", loginResult.patient.getPassword());
                   bundle.putString("login", loginResult.patient.getLogin());
                   Fragment fragment=new PatientAccountFragment();
                   fragment.setArguments(bundle);
                   getParentFragmentManager().beginTransaction()

                          .replace(R.id.fragments_container, fragment)
                                    .addToBackStack("PatientAccountFragment")
                                    .commit();

              }


//        FakeClient.getClient().login(login.getText().toString(), password.getText().toString(), new Response<Login>(){
//            @Override public void onResponse(Login response) {
//                if(response.patient != null)
//                {
//
//                    getParentFragmentManager().beginTransaction()
//                            .replace(R.id.fragments_container, new PatientAccauntFragment())
//                            .addToBackStack("PatientAccauntFragment")
//                            .commit();
//                }
//                if(response.doctor != null)
//                {
//                    getParentFragmentManager().beginTransaction()
//                            .replace(R.id.fragments_container, new DoctorAccountFragment())
//                            .addToBackStack("DoctorAccountFragment")
//                            .commit();
//                }
//            }

//
//            @Override public void onFailure(Throwable t) {
//                System.out.println("Request failed: " + t);
//            }
//        });
//
//
   }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}
