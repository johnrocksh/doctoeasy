package com.webmaster.doctoeasy.Login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.webmaster.doctoeasy.Doctor.DoctorSignUpFragment;
import com.webmaster.doctoeasy.OnBackPressed;
import com.webmaster.doctoeasy.Patient.PatientSignUpFragment;
import com.webmaster.doctoeasy.R;

public class SignupFragment extends Fragment implements OnBackPressed {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_sign_up, null, false);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        view.findViewById(R.id.btnSignupPatient).setOnClickListener((v) -> {
            onPatientBtn(v);
        });

        view.findViewById(R.id.btnSignupSpecialist).setOnClickListener((v) -> {
            onSpecialistBtn(v);
        });

        view.findViewById(R.id.btnSignupClinic).setOnClickListener((v) -> {
            onClinicBtn(v);
        });

        view.findViewById(R.id.btnBackSignUp).setOnClickListener((v) -> {
            onArrowBackBtn(v);
        });

        TextView headerText = view.findViewById(R.id.headerText);
        headerText.setText(R.string.text_signup);
    }

    public void onArrowBackBtn(View view) {
        onBackPressed();
    }


    public void onPatientBtn(View view) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientSignUpFragment())
                .addToBackStack("PatientSignUpFragment")
                .commit();
    }

    public void onSpecialistBtn(View view) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new DoctorSignUpFragment())
                .addToBackStack("DoctorSignUpFragment")
                .commit();
    }

    public void onClinicBtn(View view) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new SignupClinicFragment())
                .addToBackStack("SignupClinicFragment")
                .commit();

    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}