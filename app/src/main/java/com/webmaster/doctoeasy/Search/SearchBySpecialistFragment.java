package com.webmaster.doctoeasy.Search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.webmaster.doctoeasy.Healthcheck.HealthCheckFragment;
import com.webmaster.doctoeasy.OnBackPressed;
import com.webmaster.doctoeasy.Patient.PatientAccountFragment;
import com.webmaster.doctoeasy.Patient.PatientAppointmentFragment;
import com.webmaster.doctoeasy.Patient.PatientInboxFragment;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.client.FakeClient;

public class SearchBySpecialistFragment extends Fragment  implements OnBackPressed {
    ConstraintLayout mConstraintLayout;
    TextView textViewHeaderCAption;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.search_by_specialist_fragment, container, false);
        return v;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        initActivity(view);

        //----------------------------------------------------------
        ListView lvDoctorsSpecialisations=view.findViewById(R.id.lv_doctorsSpecialisation);
        lvDoctorsSpecialisations.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        ArrayAdapter<String> adapter= new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, FakeClient.getClient(getContext()).doctorsSpecializations);
        lvDoctorsSpecialisations.setAdapter(adapter);


        //обрабатываем нажатие
        lvDoctorsSpecialisations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedFromList = (String) (lvDoctorsSpecialisations.getItemAtPosition(position));
                Toast.makeText(getContext(), selectedFromList, Toast.LENGTH_SHORT).show();
            }});

        view.findViewById(R.id.topPanelBackButton).setOnClickListener((v->{
            onBackButtonClick(v);
        }));

        //////////////////
        //меняем цвет фона кнопки в меню
        view.findViewById(R.id.btnPatientSearch).setBackgroundResource(R.drawable.bottom_menu_pressed_bg);


        //обработка нижнего меню
        view.findViewById(R.id.btnPatientAccount).setOnClickListener((v -> {
            onbtnPatientAccountClick(v);
        }));

        view.findViewById(R.id.btnPatientHealthCheck).setOnClickListener((v->{
            onbtnPatientHealthCheckClick(v);
        }));

        view.findViewById(R.id.btnPatientInbox).setOnClickListener((v->{
            onbtnPatientInboxClick(v);
        }));

        view.findViewById(R.id.btnPatientAppointment).setOnClickListener((v -> {
            onbtnPatientAppointmentClick(v);
        }));


        /////////////////
    }

    private void onBackButtonClick(View v) {
        getParentFragmentManager().popBackStack();
    }

    private void onbtnPatientAppointmentClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientAppointmentFragment())
                .addToBackStack("PatientAppointmentFragment")
                .commit();
    }

    private void onbtnPatientInboxClick(View v) {

        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientInboxFragment())
                .addToBackStack("PatientInboxFragment")
                .commit();
    }

    private void onbtnPatientHealthCheckClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new HealthCheckFragment())
                .addToBackStack("HealthCheckFragment")
                .commit();
    }



    private void onbtnPatientAccountClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientAccountFragment())
                .addToBackStack("PatientAccauntFragment")
                .commit();
    }

    private void initActivity(View view) {

        textViewHeaderCAption=view.findViewById(R.id.textViewHeaderCaption);
        textViewHeaderCAption.setText(R.string.search_header_caption_specialist);
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}
