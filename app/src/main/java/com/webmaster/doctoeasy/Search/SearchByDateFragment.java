package com.webmaster.doctoeasy.Search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.webmaster.doctoeasy.Healthcheck.HealthCheckFragment;
import com.webmaster.doctoeasy.OnBackPressed;
import com.webmaster.doctoeasy.Patient.PatientAccountFragment;
import com.webmaster.doctoeasy.Patient.PatientAppointmentFragment;
import com.webmaster.doctoeasy.Patient.PatientInboxFragment;
import com.webmaster.doctoeasy.R;

public class SearchByDateFragment extends Fragment implements OnBackPressed {
    ConstraintLayout mConstraintLayout;

    TextView textViewCaption;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.search_by_date_fragment, container, false);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        view.findViewById(R.id.topPanelBackButton).setOnClickListener((v->{
            OnBackButtonClick(v);
        }));
        //инициализация
        initActivity(view);
        //////////////////
        //меняем цвет фона кнопки в меню
        view.findViewById(R.id.btnPatientSearch).setBackgroundResource(R.drawable.bottom_menu_pressed_bg);


        //обработка нижнего меню
        view.findViewById(R.id.btnPatientAccount).setOnClickListener((v -> {
            onbtnPatientAccountClick(v);
        }));

        view.findViewById(R.id.btnPatientHealthCheck).setOnClickListener((v->{
            onbtnPatientHealthCheckClick(v);
        }));

        view.findViewById(R.id.btnPatientInbox).setOnClickListener((v->{
            onbtnPatientInboxClick(v);
        }));

        view.findViewById(R.id.btnPatientAppointment).setOnClickListener((v -> {
            onbtnPatientAppointmentClick(v);
        }));


        /////////////////

    }

    private void OnBackButtonClick(View v) {
        getParentFragmentManager().popBackStack();
    }


    private void onbtnPatientAppointmentClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientAppointmentFragment())
                .addToBackStack("PatientAppointmentFragment")
                .commit();
    }

    private void onbtnPatientInboxClick(View v) {

        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientInboxFragment())
                .addToBackStack("PatientInboxFragment")
                .commit();
    }

    private void onbtnPatientHealthCheckClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new HealthCheckFragment())
                .addToBackStack("HealthCheckFragment")
                .commit();
    }



    private void onbtnPatientAccountClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientAccountFragment())
                .addToBackStack("PatientAccauntFragment")
                .commit();
    }

    private void initActivity(View view) {

        textViewCaption=view.findViewById(R.id.textViewHeaderCaption);
        textViewCaption.setText(R.string.search_header_caption_location);

    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}
