package com.webmaster.doctoeasy.Search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.webmaster.doctoeasy.Healthcheck.HealthCheckFragment;
import com.webmaster.doctoeasy.Patient.PatientAccountFragment;
import com.webmaster.doctoeasy.Patient.PatientAppointmentFragment;
import com.webmaster.doctoeasy.Patient.PatientInboxFragment;
import com.webmaster.doctoeasy.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SearchResultFilterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchResultFilterFragment<Qverride> extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public SearchResultFilterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchResultFilterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchResultFilterFragment newInstance(String param1, String param2) {
        SearchResultFilterFragment fragment = new SearchResultFilterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_result_filter, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


        view.findViewById(R.id.topPanelBackButton).setOnClickListener((v->{
            onBackButtonClick(v);
        }));



        //////////////////
        //меняем цвет фона кнопки в меню
        view.findViewById(R.id.btnPatientSearch).setBackgroundResource(R.drawable.bottom_menu_pressed_bg);


        //обработка нижнего меню
        view.findViewById(R.id.btnPatientAccount).setOnClickListener((v -> {
            onbtnPatientAccountClick(v);
        }));

        view.findViewById(R.id.btnPatientHealthCheck).setOnClickListener((v->{
            onbtnPatientHealthCheckClick(v);
        }));

        view.findViewById(R.id.btnPatientInbox).setOnClickListener((v->{
            onbtnPatientInboxClick(v);
        }));

        view.findViewById(R.id.btnPatientAppointment).setOnClickListener((v -> {
            onbtnPatientAppointmentClick(v);
        }));


        /////////////////
    }

    private void onBackButtonClick(View v) {

        getParentFragmentManager().popBackStack();
    }

    private void onbtnPatientAppointmentClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientAppointmentFragment())
                .addToBackStack("PatientAppointmentFragment")
                .commit();
    }

    private void onbtnPatientInboxClick(View v) {

        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientInboxFragment())
                .addToBackStack("PatientInboxFragment")
                .commit();
    }

    private void onbtnPatientHealthCheckClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new HealthCheckFragment())
                .addToBackStack("HealthCheckFragment")
                .commit();
    }



    private void onbtnPatientAccountClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientAccountFragment())
                .addToBackStack("PatientAccauntFragment")
                .commit();
    }
}