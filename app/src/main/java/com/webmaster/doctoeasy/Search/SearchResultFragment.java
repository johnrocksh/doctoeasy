package com.webmaster.doctoeasy.Search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.webmaster.doctoeasy.Doctor.DoctorDetailsFragment;
import com.webmaster.doctoeasy.Healthcheck.HealthCheckFragment;
import com.webmaster.doctoeasy.Patient.PatientAccountFragment;
import com.webmaster.doctoeasy.Patient.PatientAppointmentFragment;
import com.webmaster.doctoeasy.Patient.PatientBookingFragment;
import com.webmaster.doctoeasy.Patient.PatientInboxFragment;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.SpecialistAdapter;
import com.webmaster.doctoeasy.client.FakeClient;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchResultFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public SearchResultFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
             return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.search_result_fragment, container, false);
        return v;
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState){

        ListView listView=view.findViewById(R.id.lv_doctorsSpecialisation);

        //fill the DoctorSearchResult ListView using FakeClient
        SpecialistAdapter adapter=new SpecialistAdapter(getContext(),R.layout.specialist_items,FakeClient.getClient(getContext()).getDoctorsList(getContext(),view));
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
          @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

              ///////////
              //Put selected List View position in Bundle for geting it in doctorDetailsFragment
              DoctorDetailsFragment doctorDetailsFragment = new DoctorDetailsFragment ();
              Bundle args = new Bundle();
              args.putString("position", String.valueOf(position));
              doctorDetailsFragment.setArguments(args);
              //////////
                getParentFragmentManager().beginTransaction()

                        .replace(R.id.fragments_container,doctorDetailsFragment)
                        .addToBackStack(("DoctorDetailsFragment"))
                        .commit();
            }
        });

        view.findViewById(R.id.btnBackFragment2).setOnClickListener((v->{
            onBackBtnClicl(v);
        }));


        view.findViewById(R.id.btnPatientSearch).setBackgroundResource(R.drawable.bottom_menu_pressed_bg);


        view.findViewById(R.id.btnPatientAccount).setOnClickListener((v -> {
            onbtnPatientAccountClick(v);
        }));

        view.findViewById(R.id.btnPatientHealthCheck).setOnClickListener((v->{
            onbtnPatientHealthCheckClick(v);
        }));

        view.findViewById(R.id.btnPatientInbox).setOnClickListener((v->{
            onbtnPatientInboxClick(v);
        }));

        view.findViewById(R.id.btnPatientAppointment).setOnClickListener((v -> {
            onbtnPatientAppointmentClick(v);
        }));

    }

    private void onBackBtnClicl(View v) {
        getParentFragmentManager().popBackStack();
    }

    private void onBtnBookClick() {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientBookingFragment())
                .addToBackStack("PatientBookingFragment")
                .commit();
    }


    private void onbtnPatientAppointmentClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientAppointmentFragment())
                .addToBackStack("PatientAppointmentFragment")
                .commit();
    }

    private void onbtnPatientInboxClick(View v) {

        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientInboxFragment())
                .addToBackStack("PatientInboxFragment")
                .commit();
    }

    private void onbtnPatientHealthCheckClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new HealthCheckFragment())
                .addToBackStack("HealthCheckFragment")
                .commit();
    }



    private void onbtnPatientAccountClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientAccountFragment())
                .addToBackStack("PatientAccauntFragment")
                .commit();
    }







}