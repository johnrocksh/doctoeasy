package com.webmaster.doctoeasy.Search;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.webmaster.doctoeasy.Healthcheck.HealthCheckFragment;
import com.webmaster.doctoeasy.Patient.PatientAccountFragment;
import com.webmaster.doctoeasy.Patient.PatientAppointmentFragment;
import com.webmaster.doctoeasy.Patient.PatientInboxFragment;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.client.FakeClient;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SearchFragment extends Fragment implements AdapterView.OnItemSelectedListener {
Spinner spinBySpecialist;
Spinner spinByLocation;
TextView searchByDate;

    final Calendar myCalendar = Calendar.getInstance();
    public SearchFragment() {

    }
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.search_fragment, container, false);

        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState){

    spinBySpecialist=view.findViewById(R.id.spinnerSearchBySpecialist);
    spinByLocation=view.findViewById(R.id.spinnerSearchByLocation);

        //SEARCH BY CPECIALIST
        ArrayAdapter<String>adapterSpecialist=new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, FakeClient.getClient(getContext()).doctorsSpecializations);
        adapterSpecialist.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinBySpecialist.setAdapter(adapterSpecialist);
        spinBySpecialist.setOnItemSelectedListener(this);
        //SEARCH BY CITY
        ArrayAdapter<String>adapterLocation=new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, FakeClient.getClient(getContext()).doctorLocations);
        adapterLocation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinByLocation.setAdapter(adapterLocation);
        spinByLocation.setOnItemSelectedListener(this);

        //меняем цвет фона кнопки в меню
        view.findViewById(R.id.btnPatientSearch).setBackgroundResource(R.drawable.bottom_menu_pressed_bg);
        searchByDate = view.findViewById(R.id.edtSearchByDate);

        //обработка нижнего меню
        view.findViewById(R.id.btnPatientAccount).setOnClickListener((v -> {
            onbtnPatientAccountClick(v);
        }));

        view.findViewById(R.id.btnPatientHealthCheck).setOnClickListener((v->{
            onbtnPatientHealthCheckClick(v);
        }));

        view.findViewById(R.id.btnPatientInbox).setOnClickListener((v->{
            onbtnPatientInboxClick(v);
        }));

        view.findViewById(R.id.btnPatientAppointment).setOnClickListener((v -> {
            onbtnPatientAppointmentClick(v);
        }));


        view.findViewById(R.id.btnNextArrow).setOnClickListener(v->{
            onNextArrowBtn(v);
        });


        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, day);
                updateLabel();
            }

            private void updateLabel() {
                String myFormat = "MM.dd.yy";
                SimpleDateFormat dateFormat = new SimpleDateFormat(myFormat, Locale.US);
                searchByDate.setText(dateFormat.format(myCalendar.getTime()));
            }
        };

        searchByDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(), date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    private void onbtnPatientAppointmentClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientAppointmentFragment())
                .addToBackStack("PatientAppointmentFragment")
                .commit();
    }

    private void onbtnPatientInboxClick(View v) {

        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientInboxFragment())
                .addToBackStack("PatientInboxFragment")
                .commit();
    }

    private void onbtnPatientHealthCheckClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new HealthCheckFragment())
                .addToBackStack("HealthCheckFragment")
                .commit();
    }



    private void onbtnPatientAccountClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientAccountFragment())
                .addToBackStack("PatientAccauntFragment")
                .commit();
    }

    private void onNextArrowBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new SearchResultFragment())
                .addToBackStack("SearchResultFragment")
                .commit();
    }

    private void onSearchByDate(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new SearchByDateFragment())
                .addToBackStack("SearchByDateFragment")
                .commit();
    }

    private void onSearchByLocation(View v) {

        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new SearchByLocationFragment())
                .addToBackStack("SearchByLocationFragment")
                .commit();
    }

    private void onSearchBySpecialist(View v) {

//        getParentFragmentManager().beginTransaction()
//                .replace(R.id.fragments_container,new SearchBySpecialistFragment())
//                .addToBackStack("SearchBySpecialisFragment")
//                .commit();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(getContext(), "ok", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}