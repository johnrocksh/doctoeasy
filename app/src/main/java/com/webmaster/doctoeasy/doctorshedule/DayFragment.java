package com.webmaster.doctoeasy.doctorshedule;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.webmaster.doctoeasy.R;
/*
при загрузки фрагмента я считываю из SharedPreferences массив с состояниями кнопок
массив типа Int. если 0 то кнопка не нажата если 1 то нажата
при нажатии я меняю состояние кнопки записываю это опять же в массив и в преферецы кидаю
*/

/**
 * A simple {@link Fragment} subclass.
 */
public class DayFragment extends Fragment implements View.OnClickListener {

    private FragmentListener listener;//интерфейс для свех вьюшек используется для сбора данных и вывод в главной активити

    /*id всех кнопок*/
    Button
            btn_mon_8am, btn_mon_9am, btn_mon_10am, btn_mon_11am, btn_mon_12am, btn_mon_1pm, btn_mon_2pm, btn_mon_3pm,
            btn_mon_4pm, btn_mon_5pm, btn_mon_6pm, btn_mon_7pm, btn_mon_8pm;

    LinearLayout linearLayoutBtnTime;
    View view;

    int[] buttonsState = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    SharedPreferences pref;
    String day;


 public DayFragment(){

 }
public DayFragment(String day,FragmentListener fl){
        this.day=day;
        listener=fl;
}

//
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String selectedDay = this.day;

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_days_fragments, container, false);


        btn_mon_8am = view.findViewById(R.id.btn_mon_8am);
        btn_mon_9am = view.findViewById(R.id.btn_mon_9am);
        btn_mon_10am = view.findViewById(R.id.btn_mon_10am);
        btn_mon_11am = view.findViewById(R.id.btn_mon_11am);
        btn_mon_12am = view.findViewById(R.id.btn_mon_12am);
        btn_mon_1pm = view.findViewById(R.id.btn_mon_1pm);
        btn_mon_2pm = view.findViewById(R.id.btn_mon_2pm);
        btn_mon_3pm = view.findViewById(R.id.btn_mon_3pm);
        btn_mon_4pm = view.findViewById(R.id.btn_mon_4pm);
        btn_mon_5pm = view.findViewById(R.id.btn_mon_5pm);
        btn_mon_6pm = view.findViewById(R.id.btn_mon_6pm);
        btn_mon_7pm = view.findViewById(R.id.btn_mon_7pm);
        btn_mon_8pm = view.findViewById(R.id.btn_mon_8pm);

        btn_mon_8am.setOnClickListener(this);
        btn_mon_9am.setOnClickListener(this);
        btn_mon_10am.setOnClickListener(this);
        btn_mon_11am.setOnClickListener(this);
        btn_mon_12am.setOnClickListener(this);
        btn_mon_1pm.setOnClickListener(this);
        btn_mon_2pm.setOnClickListener(this);
        btn_mon_3pm.setOnClickListener(this);
        btn_mon_4pm.setOnClickListener(this);
        btn_mon_5pm.setOnClickListener(this);
        btn_mon_6pm.setOnClickListener(this);
        btn_mon_7pm.setOnClickListener(this);
        btn_mon_8pm.setOnClickListener(this);

        String prefStr=selectedDay+"Pref"; //Monday+Pref=MondayPref
        linearLayoutBtnTime = view.findViewById(R.id.linearLayoutBtnDays);

        pref = view.getContext().getSharedPreferences(prefStr, 0); //geting MondayPref

        try{

            //при загрузке фрагмента нужно обновить состояние кнопок согласно тому что было сохранено в SharedPreferences
            buttonsState = getButtonsState(pref,buttonsState);
            changeButtonsColorByState(buttonsState);
            listener.onUpdateSchedule(selectedDay, buttonsState);//используя интерфейс передаем данные

        }
        catch (Exception ex){
            Log.w("DEBUG", "null");
        }

        return view;

    }

    void saveButtonsStatus(int[] btnStateArray) {

        for (int i = 0; i < btnStateArray.length - 1; i++) {

            pref.edit().putInt("btn" + i, btnStateArray[i]).apply();
        }
    }

   public int[] getButtonsState(SharedPreferences pref, int[] btnStateArray) {

        /*вытаскиваем из преференсов сохраненные состояния кнопок*/
        if (pref != null) {

            for (int i = 0; i < btnStateArray.length - 1; i++) {
                try {
                    int btn_state = pref.getInt("btn" + i, btnStateArray[i]);
                    btnStateArray[i] = btn_state;
                } catch (NullPointerException ex) {
               return null;
                }
            }
        }
        return btnStateArray;
    }


    void changeButtonState(int btn_index) {
        /*если кнопка не нажато то нажимаем ее и изменяем статус*/
        if (buttonsState[btn_index] == 0) {
            buttonsState[btn_index] = 1;
        } else {
            buttonsState[btn_index] = 0;
        }
    }


    void changeButtonsColorByState(int[] btnStateArray) {


        /*восстанавливаем состояния кнопок*/
        ViewGroup viewGroup = linearLayoutBtnTime;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            Button child = (Button) viewGroup.getChildAt(i);
            if (btnStateArray[i] == 1) {
                child.setBackgroundResource(android.R.drawable.btn_default);
                child.setTextColor(getResources().getColor(R.color.colorDoctorScheduleBusyTimeText));
            } else {
                int imgID = getResources().getIdentifier("doctor_shedule_buton_pressed", "drawable",  view.getContext().getPackageName());
                child.setBackgroundResource(imgID);
                child.setTextColor(getResources().getColor(R.color.colorDoctorScheduleFreeTime));
            }
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_mon_8am: {

                changeButtonState(0);
                break;

            }
            case R.id.btn_mon_9am: {
                changeButtonState(1);
                break;
            }
            case R.id.btn_mon_10am: {
                changeButtonState(2);
                break;

            }
            case R.id.btn_mon_11am: {
                changeButtonState(3);
                break;

            }
            case R.id.btn_mon_12am: {
                changeButtonState(4);
                break;

            }
            case R.id.btn_mon_1pm: {
                changeButtonState(5);
                break;

            }

            case R.id.btn_mon_2pm: {

                changeButtonState(6);
                break;

            }
            case R.id.btn_mon_3pm: {
                changeButtonState(7);
                break;

            }

            case R.id.btn_mon_4pm: {

                changeButtonState(8);
                break;

            }
            case R.id.btn_mon_5pm: {

                changeButtonState(9);
                break;
            }
            case R.id.btn_mon_6pm: {

                changeButtonState(10);
                break;

            }

            case R.id.btn_mon_7pm: {

                changeButtonState(11);
                break;
            }
            case R.id.btn_mon_8pm: {
                changeButtonState(12);
                break;

            }

        }
        try {
            listener.onUpdateSchedule(this.day, buttonsState);//используя интерфейс передаем данные в главную активити
           // printButtonsState(buttonsState);
            saveButtonsStatus(buttonsState);
            changeButtonsColorByState(buttonsState);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
