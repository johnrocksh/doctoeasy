package com.webmaster.doctoeasy.doctorshedule;


public interface FragmentListener {
    //call this method when want to update the doctor week schedule
    void onUpdateSchedule(String weekDay, int[] buttonsState);
}

