package com.webmaster.doctoeasy.doctorshedule;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.webmaster.doctoeasy.R;

import java.util.ArrayList;
import java.util.HashMap;

//  - DoctorScheduleClass
//  - responsible for working with the schedule of doctors ..
//  - saving the schedule .. get the schedule and change ..
//  -
//  -
 public class DoctorScheduleManager  {

   private ArrayList<String> listSchedule=new ArrayList<String>();

    private HashMap<Integer,String> hashMapWeekDays = new HashMap<>();
    SharedPreferences pref;
    private   int[]              btnScheduleState;

    View schedulerView;

    //constructor
       public DoctorScheduleManager(View shedulerView){
           this.schedulerView = shedulerView;
           Log.i("schedule","DoctorScheduleManager  constructor");

           btnScheduleState =    new   int[]{0,0,0,0,0,0,0,0,0,0,0,0,0};

       hashMapWeekDays.put      (0, "8:00am"  );
       hashMapWeekDays.put      (1, "9:00am"  );
       hashMapWeekDays.put      (2, "10:00am" );
       hashMapWeekDays.put      (3, "11:00am" );
       hashMapWeekDays.put      (4, "12:00am" );
       hashMapWeekDays.put      (5, "1:00pm"  );
       hashMapWeekDays.put      (6, "2:00pm"  );
       hashMapWeekDays.put      (7, "3:00pm"  );
       hashMapWeekDays.put      (8, "4:00pm"  );
       hashMapWeekDays.put      (9, "5:00pm"  );
       hashMapWeekDays.put      (10, "6:00pm" );
       hashMapWeekDays.put      (11, "7:00pm" );
       hashMapWeekDays.put      (12, "8:00pm" );
       hashMapWeekDays.put      (13, "9:00pm" );

   }


    public void  onUpdateSchedule(Context context,String weekDay, int[] btnScheduleState) {
        Log.i("schedule","onUpdateSchedule  weekDay:"+weekDay);

       switch (weekDay) {
            case "Monday": {
                updateWorkTimeDiapason(context,"Monday:", R.id.textViewMonday, btnScheduleState);
                break;
            }
            case "Tuesday": {
                updateWorkTimeDiapason(context,"Tuesday: ", R.id.textViewTuesday, btnScheduleState);
                break;
            }
            case "Wednesday": {
                updateWorkTimeDiapason(context,"Wednesday:", R.id.textViewWednesday, btnScheduleState);
                break;
            }
            case "Thursday": {
                updateWorkTimeDiapason(context,"Thursday:", R.id.textViewThursday, btnScheduleState);
                break;
            }
            case "Friday": {
                updateWorkTimeDiapason(context,"Friday:", R.id.textViewFriday, btnScheduleState);
                break;
            }
            case "Saturday": {
                updateWorkTimeDiapason(context,"Saturday:", R.id.textViewSaturday, btnScheduleState);
                break;
            }
            case "Sunday": {
                updateWorkTimeDiapason(context,"Sunday:", R.id.textViewSunday, btnScheduleState);
                break;
            }

        }

    }

    public void updateWorkTimeDiapason(Context context, String day, int fieldId, int[] buttonsStateArray) {
        String resultStringDaySchedule;
        StringBuilder from = new StringBuilder();
        StringBuilder to = new StringBuilder();

        //если мы нашли значение  'from' то  переходим к поиску значения 'to'
        boolean fl_From = false;

        //находим начало рабочего дня и конец
        for (int i = 0; i < buttonsStateArray.length ; i++) {
            if (buttonsStateArray[i] == 1) {
                if (!fl_From) {
                    //берем индекс первого дня
                    from.append(context.getString(R.string.text_from)).append(" ").append(hashMapWeekDays.get(i)).append(" ");
                    to.append(context.getString(R.string.text_to)).append(" ").append(hashMapWeekDays.get(i)).append(" ");
                    fl_From = true;
                    } else {
                        to = new StringBuilder(context.getString(R.string.text_to)+" "+ hashMapWeekDays.get(i));
                    }
            }

            //если флаг fl_From не изменился это значит что ни одна кнопка не нажата
            // в этот день доктор не работает
            resultStringDaySchedule=day+from+to;
            updateField(fieldId, resultStringDaySchedule);
        }

        if (!fl_From) {
            updateField(fieldId, day+"free day");
        }
    }

    public void updateField(int id, String text)
    {
        TextView tv = schedulerView.findViewById(id);
        if(tv != null)
            tv.setText(text);
    }

}
