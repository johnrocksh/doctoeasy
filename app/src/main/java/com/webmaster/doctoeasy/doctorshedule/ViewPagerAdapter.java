package com.webmaster.doctoeasy.doctorshedule;

        import androidx.annotation.NonNull;
        import androidx.annotation.Nullable;
        import androidx.fragment.app.Fragment;
        import androidx.fragment.app.FragmentManager;
        import androidx.fragment.app.FragmentPagerAdapter;
        import androidx.fragment.app.FragmentStatePagerAdapter;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    FragmentListener fl;

public ViewPagerAdapter(FragmentManager fm, FragmentListener fl){
    super(fm);
    this.fl=fl;

}

    public ViewPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position){

            case 0:{
////
                return   new DayFragment("Monday",fl);

            }

            case 1:{
//
                return   new DayFragment("Tuesday",fl);

            }

            case 2:{
//
                return   new DayFragment("Wednesday",fl);
            }

            case 3:{
//
                return   new DayFragment("Thursday",fl);

            }

            case 4: {
//
                return   new DayFragment("Saturday",fl);
            }

            case 5:{
//
                return   new DayFragment("Sunday",fl);

            }

            case 6:{
//
                return   new DayFragment("Friday",fl);

                }

        }
        return null;

    }

    @Override
    public int getCount() {
        return 7;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        switch(position){

            case 0: return "Пн";
            case 1: return "Вт";
            case 2: return "Ср";
            case 3: return "Чт";
            case 4: return "Пт";
            case 5: return "Сб";
            case 6: return "Вс";


        }
        return null;
    }
}


