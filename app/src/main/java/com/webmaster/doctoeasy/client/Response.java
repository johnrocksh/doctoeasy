package com.webmaster.doctoeasy.client;

public interface Response<T> {
    void onResponse(T response);
    void onFailure(Throwable t);
}
