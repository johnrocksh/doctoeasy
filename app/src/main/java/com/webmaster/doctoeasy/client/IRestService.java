package com.webmaster.doctoeasy.client;


import com.webmaster.doctoeasy.Login.Login;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface IRestService {

    String ENDPOINT = "http://docto.webpower.cf/";

    @POST("/API")
    @FormUrlEncoded
    Call<Login> login(@Field("login") final String login, @Field("password") final String password);
}
