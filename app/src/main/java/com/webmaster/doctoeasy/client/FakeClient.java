package com.webmaster.doctoeasy.client;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.webmaster.doctoeasy.Constants;
import com.webmaster.doctoeasy.Doctor.Doctor;
import com.webmaster.doctoeasy.Login.Login;
import com.webmaster.doctoeasy.Patient.Patient;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.Specialist;
import com.webmaster.doctoeasy.doctorshedule.DoctorScheduleManager;

import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.ArrayList;


public class FakeClient {
    private static FakeClient client = null;
    private static int waitTime = 1000; // milliseconds
    private static boolean useRetrofit = false;
    DoctorScheduleManager dsm;
    public SharedPreferences mPrefs;
    public ArrayList<Specialist> specialists=new ArrayList<>();
    ArrayList<Patient> arrlstRegisteredPatient;
    ArrayList<Doctor> arrlstRegisteredDoctor;
    SharedPreferences prefPatient;
    SharedPreferences prefDoctor;
 public   String[] doctorsSpecializations={"Allergists","Dermatologists","Ophthalmologists","Obstetrician","Cardiologists","Endocrinologists","Gastroenterologists",
            "Nephrologists","Urologists","Pulmonologists"};
public String[] doctorLocations={"Chișinău", "Bălți", "Cahul"," Comrat", "Edineț", "Fălești", "Grigoriopol", "Hîncești",
        "Ialoveni", "Leova", "Mărculești","Nisporeni", "Ocnița", "Orhei", "Otaci", "Rezina", "Rîbnița", "Rîșcani"};

    FakeClient(Context context){
        prefPatient = context.getSharedPreferences( Constants.prefRegisteredPatient, 0); //
        prefDoctor = context.getSharedPreferences( Constants.prefRegisteredDoctor, 0); //
        arrlstRegisteredPatient = extractPatientArrayListFromPref( prefPatient,Constants.prefRegisteredPatient);
        arrlstRegisteredDoctor = extractDoctorArrayListFromPref(prefDoctor,Constants.prefRegisteredDoctor);

    }
//    FakeClient(){
//
//    }
     class UserRegistrationResult {  //if patient was registered successful then return true else return false and err_Msg
         private boolean reg_Result;
         private String err_Msg;

         UserRegistrationResult(boolean reg_Result, String err_Msg){
             this.reg_Result=reg_Result;
             this.err_Msg=err_Msg;
         }
     }


    public static FakeClient getClient(Context context)
    {
        if(client == null) {
            client = new FakeClient(context);
        }

        return client;
    }

    /////////////
    //changePatientPassword  - return true if password was changed
    ////////////
    public boolean changePatientPassword(Context context, String patientLogin, String patientNewPassword){
        boolean fl=false;
        SharedPreferences pref=context.getSharedPreferences(Constants.prefRegisteredPatient,0);
        ArrayList<Patient>arrayListPatient=extractPatientArrayListFromPref(pref,Constants.prefRegisteredPatient);
        if(arrayListPatient==null){fl=false; return fl;}

        for(int i=0;i<arrayListPatient.size();i++){
            if(arrayListPatient.get(i).getLogin().equals(patientLogin)){
               arrayListPatient.get(i).setPassword(patientNewPassword);
               fl=true;
            }
        }
        saveArrayListToPreference(arrayListPatient,pref,Constants.prefRegisteredPatient);
        return fl;
    }


    public Patient getPatient(Context context,String patientLogin){

    SharedPreferences pref=context.getSharedPreferences(Constants.prefRegisteredPatient,0);
    ArrayList<Patient> arrayListPatient= extractPatientArrayListFromPref(pref,Constants.prefRegisteredPatient);
        for(Patient p:arrayListPatient){
            if(p.getLogin().equals(patientLogin)){
                return p;
            }
        }
        return null;
    }

    public Doctor getDoctor(Context context,String doctorLogin, String doctorPassword){

        SharedPreferences pref=context.getSharedPreferences(Constants.prefRegisteredDoctor,0);
        ArrayList<Doctor> arrayListDoctor= extractDoctorArrayListFromPref(pref,Constants.prefRegisteredDoctor);
        for(Doctor d:arrayListDoctor){
            if(d.getLogin().equals(doctorLogin)&&d.getPassword().equals(doctorPassword)){
                return d;
            }
        }
        return null;
    }


    public Login login(Context context,String login, String password)
    {
      SharedPreferences prefPatient = context.getSharedPreferences( Constants.prefRegisteredPatient, 0); //
      SharedPreferences prefDoctor = context.getSharedPreferences( Constants.prefRegisteredDoctor, 0); //

         Login loginResult = new Login();
       ArrayList<Patient> arrlstRegisteredPatient = extractPatientArrayListFromPref(prefPatient, Constants.prefRegisteredPatient);
       ArrayList<Doctor> arrlstRegisteredDoctor = extractDoctorArrayListFromPref(prefDoctor, Constants.prefRegisteredDoctor);


        if(arrlstRegisteredPatient!=null){
            for (Patient p : arrlstRegisteredPatient){
                if (p.getLogin().equals(login) && p.getPassword().equals(password)) {
                    loginResult.patient = p;
                    return loginResult;
                }
            }
        }
        if(arrlstRegisteredDoctor!=null){
            for(Doctor d: arrlstRegisteredDoctor){

                if(d.getLogin().equals(login)&&d.getPassword().equals(password)){
                    loginResult.doctor=d;
                    return loginResult;
                }
            }
        }
             if (login.equals("p") && password.equals("1")) {
                    Patient patient = new Patient();
                    loginResult.patient = patient;
                 return loginResult;

                }

                  if (login.equals("d") && password.equals("1")) {
                    Doctor doctor = new Doctor();
                     loginResult.doctor = doctor;
                      return loginResult;

                }
                  return loginResult;
            }

    /////////////
    // login Retrofit
    ////////////
//    public void login(String login, String password, Response<Login> handler)
//    {
////        if(useRetrofit) {
////            Call<Login> loginRequest = RestClient.getClient().login(login, password);
////
////            loginRequest.enqueue(new Callback<Login>() {
////
////                @Override
////                public void onResponse(Call<Login> call, retrofit2.Response<Login> response) {
////                    handler.onResponse(response.body());
////                }
////
////                @Override
////                public void onFailure(Call<Login> call, Throwable t) {
////                    //System.out.println(call.request().url() + ": failed: " + t);
////                    handler.onFailure(t);
////                }
////            });
////        }
//        if(true) {
//            new android.os.Handler().postDelayed(() -> {
//                Login loginResult = new Login();
//                if (login.equals("p") && password.equals("1")) {
//                    Patient patient = new Patient();
//       loginResult.patient = patient;
//                }
//                if (login.equals("d") && password.equals("1")) {
//                    Doctor doctor = new Doctor();
//                    doctor.id = 2;
//                    doctor.name = "Vanea doc";
//                    loginResult.doctor = doctor;
//                }
//                handler.onResponse(loginResult);
//            }, waitTime);
//        }
//    }


    /////////////
    // New Patient Registration
    ////////////
    public UserRegistrationResult newPatientRegistration(Context context,String login, Patient newPatient){
        UserRegistrationResult userRegistration;
        boolean reg_Result=false;
        String err_Msg="";

        if(arrlstRegisteredPatient==null){arrlstRegisteredPatient=new ArrayList<>();}
        if(arrlstRegisteredDoctor==null){arrlstRegisteredDoctor=new ArrayList<>();}

            for(Patient p:arrlstRegisteredPatient){
                if(p.getLogin().equals(login)){
                    err_Msg = "User with name just registered!";
                    reg_Result = false;
                    userRegistration= new UserRegistrationResult(reg_Result, err_Msg);
                    return userRegistration;
                } else { reg_Result = true; }

                for(Doctor d:arrlstRegisteredDoctor){
                    if(d.getLogin().equals(login)){
                        err_Msg = "User with name just registered!";
                        reg_Result = false;
                        userRegistration= new UserRegistrationResult(reg_Result, err_Msg);
                        return userRegistration;
                    }else{reg_Result=true;}
                }

        }
        if(reg_Result=true){
            addNewUser(true,newPatient);
            err_Msg = "User was registered successful!";
            userRegistration= new UserRegistrationResult(reg_Result, err_Msg);
            return userRegistration;
        }
        userRegistration= new UserRegistrationResult(reg_Result, err_Msg);
        return userRegistration;
    }

    /////////////
    // New Doctor Registration
    ////////////
    public UserRegistrationResult newDoctorRegistration(Context context,String login, Doctor newDoctor){
        UserRegistrationResult userRegistration;
        boolean reg_Result=false;
        String err_Msg="";

        if(arrlstRegisteredPatient==null){arrlstRegisteredPatient=new ArrayList<>();}
        if(arrlstRegisteredDoctor==null){arrlstRegisteredDoctor=new ArrayList<>();}

        for(Patient p:arrlstRegisteredPatient){
            if(p.getLogin().equals(login)){
                err_Msg = "User with name just registered!";
            }else{reg_Result=true;}

            for(Doctor d:arrlstRegisteredDoctor){
                if(d.getLogin().equals(login)){
                    err_Msg = "User with name just registered!";
                }else{reg_Result =true;}
            }
            if(reg_Result==true){
                addNewUser(false,newDoctor);
                err_Msg = "User was registered successful!";
            }
        }
        userRegistration= new UserRegistrationResult(reg_Result, err_Msg);
        return userRegistration;
    }


    /////////////
    // addNewUser to SharedPrefferences
    ////////////

    void addNewUser(boolean isPatient,Object newUser)
    {
        Patient p; Doctor d;
    if(isPatient){
        p=(Patient)newUser;
        p.setRegistered(true);
        arrlstRegisteredPatient.add(p);
        saveArrayListToPreference(arrlstRegisteredPatient, prefPatient, Constants.prefRegisteredPatient);
    }
    else {
        d=(Doctor)newUser;
        d.setRegistered(true);
        arrlstRegisteredDoctor.add(d);
        saveDoctorArrayListToPreference(arrlstRegisteredDoctor, prefDoctor, Constants.prefRegisteredDoctor);
        }
    }

    /////////////
    // New Patient Registration
    ////////////
    public UserRegistrationResult newDoctorRegistration(Context context, Doctor newDoctor) {
        SharedPreferences pref = context.getSharedPreferences( Constants.prefRegisteredDoctor, 0); //

        boolean reg_Result = false;
        String err_Msg = "";
        ArrayList<Doctor> arrlstRegisteredDoctor = extractDoctorArrayListFromPref( pref,Constants.prefRegisteredDoctor);
        if(arrlstRegisteredDoctor!=null){//if arrlstRegisteredPatient just created then write to it new patient

            for (Doctor d : arrlstRegisteredDoctor) {           // if patient is not registered yet then register it
                if (d.getName().equals(newDoctor.getName()) && d.getLastName().equals(newDoctor.getLastName())) {
                    err_Msg = "Doctor with name just registered!";
                    reg_Result = false;
                } else {
                    newDoctor.setRegistered(true);
                    arrlstRegisteredDoctor.add(newDoctor);
                    saveDoctorArrayListToPreference(arrlstRegisteredDoctor, pref, Constants.prefRegisteredDoctor   );
                    err_Msg = "New patient was registered with big success!";
                    reg_Result = true;
                }
            }
        }
        else{ //if arrlstRegisteredPatient==null then create it and write to it new patient
            arrlstRegisteredDoctor=new ArrayList<Doctor>();
            newDoctor.setRegistered(true);
            arrlstRegisteredDoctor.add(newDoctor);
            saveDoctorArrayListToPreference(arrlstRegisteredDoctor, pref, Constants.prefRegisteredDoctor);
            err_Msg = "New patient was registered with big success!";
            reg_Result = true;
        }

        return   new UserRegistrationResult(reg_Result, err_Msg);
    }

    /////////////
    //saveDoctorPhotoPath
    ////////////

    public boolean saveDoctorPhotoPath(Context context, Doctor doctor, Path path){
        SharedPreferences pref=context.getSharedPreferences(Constants.prefRegisteredDoctor,0);
        ArrayList<Doctor> arrayListDoctor = extractDoctorArrayListFromPref(pref,Constants.prefRegisteredDoctor);
        for(int i =0;i<arrayListDoctor.size();i++){
            if(arrayListDoctor.get(i).getLogin().equals(doctor.getLogin())&&arrayListDoctor.get(i).getPassword().equals(doctor.getPassword())){
                arrayListDoctor.get(i).setPhotoPath(path.toString());
            }
            saveDoctorArrayListToPreference(arrayListDoctor,pref,Constants.prefRegisteredDoctor);
        }

        return true;
    }


    /////////////
    // savePhotoPatient
    ////////////

    public boolean savePhotoPathInPatient(Context context, Patient patient, Path path){
        SharedPreferences pref=context.getSharedPreferences(Constants.prefRegisteredPatient,0);
        ArrayList<Patient> arrayListPatient = extractPatientArrayListFromPref(pref,Constants.prefRegisteredPatient);
        for(int i =0;i<arrayListPatient.size();i++){
            if(arrayListPatient.get(i).getLogin().equals(patient.getLogin())&&arrayListPatient.get(i).getPassword().equals(patient.getPassword())){
               arrayListPatient.get(i).setPhotoPath(path.toString());
            }
      saveArrayListToPreference(arrayListPatient,pref,Constants.prefRegisteredPatient);
        }

        return true;
    }

    /////////////
    // extractPatientArrayListFromPref
    ////////////.
    public ArrayList<Patient> extractPatientArrayListFromPref(SharedPreferences mPrefs, String prefName) {

        //  Gson gson = new Gson();                                                       Log.i("LOG", "gson"+gson);
        String json = mPrefs.getString(prefName, "");                        Log.i("LOG", "String json:"+json);
        Type listType= new TypeToken<ArrayList<Patient>>(){}.getType();
        ArrayList<Patient> patientArrayList = new Gson().fromJson(json,listType);
        return patientArrayList;

    }
    /////////////
    // extractDoctorArrayListFromPref
    ////////////.
    public ArrayList<Doctor> extractDoctorArrayListFromPref(SharedPreferences mPrefs, String prefName) {

        //  Gson gson = new Gson();                                                       Log.i("LOG", "gson"+gson);
        String json = mPrefs.getString(prefName, "");                        Log.i("LOG", "String json:"+json);
        Type listType= new TypeToken<ArrayList<Doctor>>(){}.getType();
        ArrayList<Doctor> doctorArrayList = new Gson().fromJson(json,listType);
        return doctorArrayList;

    }

    /////////////
    // saveArrayListToPreference
    ////////////
    public void saveArrayListToPreference(ArrayList<Patient> arrayListPatient,SharedPreferences mPrefs, String prefName) {
        if(mPrefs!=null){
            SharedPreferences.Editor prefsEditor = mPrefs.edit();
            Log.i("LOG", "saveArrayListToPreference: ");
            Gson gson = new Gson();
            String strJson = gson.toJson(arrayListPatient);         //convert arrayListPatient to Json
            prefsEditor.putString(prefName, strJson);
            prefsEditor.commit();
        }
        else{
            Log.i("LOG", "SharedPreference /-mPref-/ is null");
        }
    }

    /////////////
    // saveArrayListToPreference
    ////////////
    public void saveDoctorArrayListToPreference(ArrayList<Doctor> arrayListDoctor,SharedPreferences mPrefs, String prefName) {
        if(mPrefs!=null){
            SharedPreferences.Editor prefsEditor = mPrefs.edit();
            Log.i("LOG", "saveArrayListToPreference: ");
            Gson gson = new Gson();
            String strJson = gson.toJson(arrayListDoctor);         //convert arrayListPatient to Json
            prefsEditor.putString(prefName, strJson);
            prefsEditor.commit();
        }
        else{
            Log.i("LOG", "SharedPreference /-mPref-/ is null");
        }
    }
    /////////////
    // setConfirmPatientAppointment
    ////////////
    public void setConfirmPatientAppointment(Patient.Status confirm,Context context,Patient patient, String strPref){

        //1 get ArrayListPatientBooking from preff. add new patient and save now
        SharedPreferences pref = context.getSharedPreferences(strPref, 0); //
        ArrayList<Patient> arrayListPatientBooking;

        if (pref!=null) {
            arrayListPatientBooking = extractPatientArrayListFromPref(pref,strPref);
            if (arrayListPatientBooking!=null) {

                int indexOfConfirmedPatient=getIndexOfConfirmedPatient(patient, arrayListPatientBooking);
                arrayListPatientBooking.get(indexOfConfirmedPatient).setStatus(confirm);
                saveArrayListToPreference(arrayListPatientBooking,pref,strPref);

            }else {
                Log.i("LOG", "Error!!! Can't find records arrayListPatientBooking in database for Confirmation");
            }
        }
    }

    /////////////
    // find patient in array list for confirm it
    ////////////
    private int getIndexOfConfirmedPatient(Patient patient, ArrayList<Patient> arrayListPatientBooking) {
        for(int i = 0; i< arrayListPatientBooking.size(); i++){
            if((arrayListPatientBooking.get(i).getName().equals(patient.getName())&&
            arrayListPatientBooking.get(i).getLastName().equals(patient.getLastName())&&
                    arrayListPatientBooking.get(i).getTimeOfBooking().equals(patient.getTimeOfBooking())&&
                            arrayListPatientBooking.get(i).getDateOfBooking().equals(patient.getDateOfBooking())))
            {
                return i;
            }
        }
        return -1;
    }

    /////////////
    // patientBooking   ////call this method when was pressed button "Ok" in PatientBookingFragment
    ////////////
    public void patientBooking(Context context,Patient patient, String strPref){

        //1 get ArrayListPatientBooking from prefference add new patient and save now
        SharedPreferences pref = context.getSharedPreferences(strPref, 0); //geting MondayPref
        ArrayList<Patient> arrayListPatientBooking;
        if (pref!=null) {

            arrayListPatientBooking = extractPatientArrayListFromPref(pref,strPref);
            if (arrayListPatientBooking!=null) {
                arrayListPatientBooking.add(patient);
                saveArrayListToPreference(arrayListPatientBooking,pref,strPref);
            }else {
                //create new arraylist and save there patient booking info
                arrayListPatientBooking=new ArrayList<Patient>();
                arrayListPatientBooking.add(patient);
                saveArrayListToPreference(arrayListPatientBooking,pref,strPref);
            }
        }
    }



    /////////////
    // getDoctorsList return filled ArraList with doctors(specialist)
    ////////////
    public ArrayList getDoctorsList(Context context, View view){
            specialists.clear();
           //первый доктор
           Drawable photoSpecialist=context.getResources().getDrawable(R.drawable.photo_specialist1);
           String name="Robert Deniro";
           String specialisation="Casino";
           String address="Manhattan, New York City";
           Float latitude=40.730610f;
           Float longitude=-73.971321f;
           int   stars=1;

           //второй доктор
           Drawable photoSpecialist2=context.getResources().getDrawable(R.drawable.photo_specialist2);
           String name2="Silvestr S";
           String specialisation2="boxer";
      String address2="New York City";
      Float latitude2=40.730610f;
      Float longitude2=-73.935242f;
      int   stars2=3;

      //третий доктор
      Drawable photoSpecialist3=context.getResources().getDrawable(R.drawable.photo_specialist3);
      String name3="Arnold Shwarc";
      String specialisation3="terminator";
      String address3="Austria";
      Float latitude3=48.210033f;
      Float longitude3=16.363449f;
      int   stars3=4;

      //четвертый доктор
      Drawable photoSpecialist4=context.getResources().getDrawable(R.drawable.photo_specialist4);
      String name4="Mikki Rurg";
      String specialisation4="mafia";
      String address4="Нью-Йорке";
      Float latitude4=40.730610f;
      Float longitude4=73.935242f;
      int   stars4=4;

      //пятый доктор
      Drawable photoSpecialist5=context.getResources().getDrawable(R.drawable.photo_specialist5);
      String name5="John Rock";
      String specialisation5="programmer";
      String address5="Moldova";
      Float latitude5=47.411633f;
      Float longitude5=28.369884f;
      int   stars5=2;

    //  starRating(view.getContext(), stars);

           specialists.add(new Specialist(photoSpecialist,name,specialisation, starRating(view.getContext(), stars),address,latitude,longitude,stars));
           specialists.add(new Specialist(photoSpecialist2,name2,specialisation2,starRating(view.getContext(), stars2),address2,latitude2,longitude2,stars2));
           specialists.add(new Specialist(photoSpecialist3,name3,specialisation3,starRating(view.getContext(), stars3),address3,latitude3,longitude3,stars3));
           specialists.add(new Specialist(photoSpecialist4,name4,specialisation4,starRating(view.getContext(), stars4),address4,latitude4,longitude4,stars4));
           specialists.add(new Specialist(photoSpecialist5,name5,specialisation5,starRating(view.getContext(), stars5),address5,latitude5,longitude5,stars5));
          return  specialists;
       }


    Drawable[] starRating(Context context,int count){
        Drawable star=context.getResources().getDrawable(R.drawable.star);
        Drawable[] starRating=new Drawable[5];
        for (int i=0;i<=count;i++){
            starRating[i]=star;
        }
        return starRating;
       }

   }



