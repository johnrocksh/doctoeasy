package com.webmaster.doctoeasy.Healthcheck;



import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;


import com.webmaster.doctoeasy.Patient.PatientAccountFragment;
import com.webmaster.doctoeasy.Patient.PatientAppointmentFragment;
import com.webmaster.doctoeasy.Patient.PatientInboxFragment;
import com.webmaster.doctoeasy.R;
import com.webmaster.doctoeasy.Search.SearchFragment;

public class HealthCheckFragment extends Fragment  {



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.health_check_fragment, container, false);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        //меняем цвет фона кнопки в меню
        view.findViewById(R.id.btnPatientHealthCheck).setBackgroundResource(R.drawable.bottom_menu_pressed_bg);

        view.findViewById(R.id.imageViewArrowBack).setOnClickListener((v->{
             OnArrowBackClick(v);
         }));

        ///////////
        view.findViewById(R.id.btnPatientAccount).setOnClickListener((v->{
            onAccountBtn(v);
        }));

        view.findViewById(R.id.btnPatientSearch).setOnClickListener((v) -> {
            onSearchBtn(v);
        });

        view.findViewById(R.id.btnPatientAppointment).setOnClickListener((v) -> {
            onAppointmentBtn(v);
        });

        view.findViewById(R.id.btnPatientInbox).setOnClickListener((v) -> {
            onInboxBtn(v);
        });

        view.findViewById(R.id.btnPatientHealthCheck).setOnClickListener((v) -> {
            onHealthBtn(v);
        });

        //////////
        view.findViewById(R.id.btnGetRecomendation).setOnClickListener((v)->{
            onBtnGetRecomendationClick(v);
        });
    }

    private void onBtnGetRecomendationClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new HealthCheckQuestionsFragment())
                .addToBackStack("HealthCheckQuestionFragment")
                .commit();
    }

    private void onAccountBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientAccountFragment())
                .addToBackStack("PatietnAccountFragment")
                .commit();
    }

    private void onHealthBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new HealthCheckFragment())
                .addToBackStack("HealthCheckFragment")
                .commit();
    }

    private void onInboxBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientInboxFragment())
                .addToBackStack("PatientInboxFragment")
                .commit();
    }



    private void onSearchBtn(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new SearchFragment())
                .addToBackStack("SearchFragment")
                .commit();
    }

    private void onAppointmentBtn(View v) {

        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new PatientAppointmentFragment())
                .addToBackStack("PatientAppointmentFragment")
                .commit();
    }

    private void OnArrowBackClick(View v) {
            getParentFragmentManager().popBackStack();
    }



}
