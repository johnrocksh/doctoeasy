package com.webmaster.doctoeasy.Healthcheck;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;


import com.webmaster.doctoeasy.R;

public class HealthCheckQuestionsFragment extends Fragment {
    private Context mContext;

    private RelativeLayout mRelativeLayout;
    private SeekBar mSeekBar;
    private TextView mTextView;
    private  ActionBar actionBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_health_check_questions, container, false);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

          // Get the application context
        mContext = view.getContext();

//        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
//        actionBar =   setSupportActionBar(mContext);

        if (actionBar != null) {
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorVelocity)));
        }

      mSeekBar = (SeekBar) view.findViewById(R.id.seekbar);
       mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                // Display the current progress of SeekBar
              //  mTextView.setText(""+i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

       view.findViewById(R.id.tvBtnOk).setOnClickListener((v->{
           onBtnOkClick(v);
       }));

       view.findViewById(R.id.btnBackFragment).setOnClickListener((v->{
           onBtnBackFragmentClick(v);
       }));
    }

    private void onBtnBackFragmentClick(View v) {
        getParentFragmentManager().popBackStack();
    }


    private void onBtnOkClick(View v) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragments_container,new HealthCheckResultFragment())
                .addToBackStack("HealthCheckFragmentManager")
                .commit();
    }


}
