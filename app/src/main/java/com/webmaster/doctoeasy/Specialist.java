package com.webmaster.doctoeasy;
import android.graphics.drawable.Drawable;
import java.util.ArrayList;

/////////////////// This class i use in feeling of ListView  DoctorSearchResult
public class Specialist {
    private Drawable photo;
    private String   name;
    private String   specialisation;
    private String   address;
    private Float    latitude;
    private Float    longitude;
    private Drawable[] starRating;
    private int starsCount;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    private String login;


    public int getStarsCount() {
        return starsCount;
    }

    public void setStarsCount(int starsCount) {
        this.starsCount = starsCount;
    }





    public ArrayList<Specialist> specialists=new ArrayList<>();


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Specialist(Drawable photo, String name, String specialisation, Drawable[] starRating
   , String address, Float latitude, Float longitude,int starsCount){

        this.name=name;
        this.photo=photo;
        this.starRating=starRating;
        this.specialisation=specialisation;
        this.address=address;
        this.latitude=latitude;
        this.longitude=longitude;
        this.starsCount=starsCount;



    }


       public Drawable[] getStarRating(){
        return starRating;
    }
    public String getName(){
        return name;

    }
    public String getSpecialisation(){
        return specialisation;
    }


    public Drawable getPhoto(){
        return photo;
    }

    public void setPhoto(Drawable photo){
        this.photo=photo;
    }

    public void setName (String name){
        this.name=name;
    }

    public   void setSpecialisation(String specialisation)
    {
        this.specialisation=specialisation;
    }


    public void setStarRating(Drawable[] starRating){
        this.starRating=starRating;
    }
}




